(function () {
    'use strict';

    var core = angular.module('app.core');

    core.config(toastrConfig);

    toastrConfig.$inject = ['toastr'];
    /* @ngInject */
    function toastrConfig(toastr) {
        toastr.options.timeOut = 4000;
        toastr.options.positionClass = 'toast-bottom-right';
    }

    var config = {
        appErrorPrefix: '[vala Error] ',
        appTitle: 'vala'
    };

    core.value('config', config);

    core.config(configure);

    configure.$inject = ['$logProvider', 'routerHelperProvider',
        'exceptionHandlerProvider'];
    /* @ngInject */
    function configure($logProvider, routerHelperProvider,
        exceptionHandlerProvider) {
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        routerHelperProvider.configure({
            docTitle: config.appTitle + ': '
        });
    }
    core.config(satellizerConfig);

    satellizerConfig.$inject = ['$httpProvider', '$authProvider', 'SITE_CONFIG'];
    /* @ngInject */
    function satellizerConfig($httpProvider, $authProvider, SITE_CONFIG) {
        $httpProvider.defaults.useXDomain = true;
        $authProvider.loginUrl = 'authenticate';
        $authProvider.baseUrl = SITE_CONFIG.API_URL;
        $authProvider.signupUrl = $authProvider.baseUrl + 'authenticate/register';
        $authProvider.facebook({
            clientId: SITE_CONFIG.FACEBOOK_CLIENT_ID,
            url: 'authenticate/facebook',
            display: 'popup',
            requiredUrlParams: ['display', 'scope'],
            scope: ['email', 'public_profile'],
            scopeDelimiter: ',',
            authorizationEndpoint: 'https://www.facebook.com/v2.5/dialog/oauth',
        });
        $authProvider.twitter({
            url: '/authenticate/twitter',
            responseType: 'token',
            authorizationEndpoint: 'https://api.twitter.com/oauth/authenticate',
            redirectUri: window.location.origin,
            type: '1.0',
            popupOptions: { width: 495, height: 645 }
        });
        $authProvider.google({
            url: 'authenticate/google',
            clientId: SITE_CONFIG.GOOGLE_CLIENT_ID,
            scope: ['openid', 'email', 'profile']
        });
        $authProvider.linkedin({
            url: 'authenticate/linkedin',
            authorizationEndpoint: 'https://www.linkedin.com/uas/oauth2/authorization',
            clientId: SITE_CONFIG.LINKEDIN_CLIENT_ID,
            redirectUri: window.location.origin,
            requiredUrlParams: ['state'],
            scope: ['r_basicprofile', 'r_emailaddress'],
            scopeDelimiter: ' ',
            state: 'STATE',
            type: '2.0',
        });
    }

    core.config(themeConfig);

    /* @ngInject */
    function themeConfig($mdThemingProvider, themePalette) {
        $mdThemingProvider.definePalette('vala2theme', themePalette);
        $mdThemingProvider.theme('default').primaryPalette('vala2theme');
        $mdThemingProvider.theme('blue').primaryPalette('blue');
        $mdThemingProvider.theme('teal').primaryPalette('teal');
        $mdThemingProvider.theme('indigo').primaryPalette('indigo');
        $mdThemingProvider.theme('orange').primaryPalette('orange');
        $mdThemingProvider.theme('green').primaryPalette('green');
        //for theming change demo
        $mdThemingProvider.alwaysWatchTheme(true);
    }

})();
