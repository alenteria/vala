(function () {
    'use strict';

    angular
        .module('app.core')
        .factory('dataservice', dataservice);

    dataservice.$inject = ['$http', '$q', 'exception', 'SatellizerConfig'];
    /* @ngInject */
    function dataservice($http, $q, exception, SatellizerConfig) {
        var baseUrl = SatellizerConfig.baseUrl,
            service = {
                getSiteConfig: getSiteConfig
            };

        return service;

        function getSiteConfig() {
            //
        }
    }
})();
