/* global toastr:false, moment:false */
(function () {
    'use strict';

    angular
        .module('app.core')
        .constant('toastr', window.toastr)
        .constant('moment', window.moment)
        .constant('$', window.jQuery)
        .constant('$lodash', window._)
        .constant('themePalette', window.themePalette || {
            '50': 'ffebee',
            '100': 'ffcdd2',
            '200': 'ef9a9a',
            '300': 'f3776f',
            '400': 'f1645d',
            '500': '009fc2',
            '600': '1ab394',
            '700': 'b63e97',
            '800': '6a2c91',
            '900': '57585a',
            'A100': 'FF8A80',
            'A200': 'FF5252',
            'A400': 'FF1744',
            'A700': 'D50000',
            'contrastDefaultColor': 'light',
            'contrastDarkColors': ['50', '100'],
            'contrastLightColors': undefined
        })
        .value('googleChartApiConfig', {
            version: '1.1',
            optionalSettings: {
                packages: ['timeline'],
                language: 'en'
            }
        });
})();
