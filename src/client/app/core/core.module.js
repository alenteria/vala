(function () {
    'use strict';

    angular
        .module('app.core', [
            'ngMaterial',
            'ngAnimate',
            'ngSanitize',

            'blocks.logger',
            'blocks.exception',
            'blocks.router',

            'ui.router',

            'satellizer',

        ]);
})();
