(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'dashboard',
                config: {
                    abstract: true,
                    templateUrl: 'app/dashboard/dashboard.html',
                    controller: 'DashboardController',
                    controllerAs: 'vm',
                    title: 'Dashboard',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-th-large"></i> Dashboard'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'dashboard.widgets',
                config: {
                    url: '/',
                    templateUrl: 'app/dashboard/views/widgets.html',
                    controller: 'DashbpardWidgetsController',
                    controllerAs: 'vm',
                    title: 'Dashboard',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-th-large"></i> Dashboard'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'dashboard.timeline',
                config: {
                    url: '/timeline',
                    templateUrl: 'app/dashboard/views/timeline.html',
                    controller: 'TimelineController',
                    controllerAs: 'vm',
                    title: 'Timeline',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-th-large"></i> Timeline'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }

})();
