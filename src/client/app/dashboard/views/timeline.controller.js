(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('TimelineController', TimelineController);

    /* @ngInject */
    function TimelineController($q,
        themePalette,
        userService,
        logger,
        $state, $scope, $timeout, $window) {
        var vm = this;

        activate();

        function activate() {
            var promises = [
                    userService.getProfile(true)
            ];
            $scope.$emit('progress.show');
            $q.all(promises).then(function (results) {

                vm.user = results[0].data;

                logger.info('Activated Dashboard View');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
            vm.time = formatAMPM(new Date());
        }

        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }
    }
})();
