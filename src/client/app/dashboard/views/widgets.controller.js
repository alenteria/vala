(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashbpardWidgetsController', DashbpardWidgetsController);

    /* @ngInject */
    function DashbpardWidgetsController($q,
        userService,
        categoryService,
        chartTypeService,
        chartDataService,
        dashboardService,
        themePalette,
        logger,
        $state, $scope, $timeout, $window) {
        var vm = this;
        vm.welcome = {
            title: 'Welcome and please meet VALA 1.0',
            content: '<p><small>This is our brand new software designed to help ' +
            'you through the nomination and application process. ' +
            'If at any time our new baby gives you trouble please ' +
            'click our Report a Challenge button to share your ' +
            'feedback with our VALA nannies.</small></p>' +
            '<p>Thank you for understanding.</p>'
        };

        vm.demoRole = function (msg, data) {
            console.log(data);
            vm.role = data.role;
        };

        vm.documentReady = false;
        vm.documentReady = true;
        vm.messageCount = 0;
        vm.people = [];
        vm.title = 'Dashboard';
        vm.page = $state.current;
        vm.regions = ['Dorset', 'Devon', 'Southhampon', 'Postsmouth', 'Isle of Wight'];
        vm.categories = [];
        vm.chartTypes = [];
        vm.selectedChartType = {};
        vm.columnChartObject = {
            type: 'ColumnChart',
            options: {
                colors: getThemePallete(4, 12),
                pointSize: 3,
                is3D: true,
                curveType: 'function',
                timeline: {
                    backgroundColor: '#F00',
                    groupByRowLabel: true,
                    colorByRowLabel: false,
                    isHtml: false
                }
            },
            annotation: {
            }
        };
        vm.pieChartObject1 = {
            type: 'PieChart',
            options: {
                colors: getThemePallete(3, vm.regions.length),
                is3D: true
            },
            view: {
                columns: [0, 1]
            }
        };
        vm.pieChartObject2 = {
            type: 'PieChart',
            options: {
                colors: getThemePallete(1, vm.regions.length),
                is3D: true
            },
            view: {
                columns: [0, 2]
            }
        };
        vm.lineChartObject = {
            type: 'AreaChart',
            options: {
                title: 'Ticket Sales per Month',
                colors: getThemePallete(4, 12)
            }
        };
        vm.pieSponsorChartObject = {
            type: 'PieChart',
            options: {
                title: 'Sponsors',
                colors: getThemePallete(0, 10)
            }
        };

        vm.setChartType = setChartType;

        activate();

        function activate() {
            var promises = [
                    userService.getProfile(true),
                    dashboardService.getEnabledUserRoleModules(),
                    categoryService.getCategoryList(),
                    chartTypeService.getChartTypeList(),
                    chartDataService.query('Ticket Sales'),
                    chartDataService.query('Sponsors')
            ];
            $scope.$emit('progress.show');
            $q.all(promises).then(function (results) {

                vm.user = results[0].data;
                vm.role = 'superadmin'; //must be included in login already
                vm.modules = results[1].data;
                vm.categories = results[2].data;
                vm.chartTypes = results[3].data;
                vm.lineChartObject.data = results[4].data;
                vm.pieSponsorChartObject.data = results[5].data;

                vm.selectedChartType = vm.chartTypes[0];

                vm.setChartType(vm.selectedChartType);

                logger.info('Activated Dashboard View');
            }).finally(function () {
                $timeout(function () {
                    $scope.$emit('progress.hide');
                }, 1000);
            });
        }

        function setChartType(chartType) {
            //set selected chart before getting data
            vm.selectedChartType = chartType;
            chartDataService.query(chartType.name).then(function (result) {
                setChartData(result.data);
            });
        }

        function setChartData(data) {
            //assign data to chart view model
            vm.columnChartObject.data = data;

            vm.columnChartObject.options.title = vm.selectedChartType.name;

            if (vm.selectedChartType.type) {
                vm.columnChartObject.type = vm.selectedChartType.type;
            }
            if (vm.selectedChartType.type === 'ColumnChart') {
                vm.pieChartObject1.data = data;
                vm.pieChartObject2.data = data;
                vm.pieChartObject1.options.title = data.cols[1].label;
                vm.pieChartObject2.options.title = data.cols[2].label;
            }
        }

        function getThemePallete(start, count) {
            var ar = [
                '4a6ab7',
                '3d67ed',
                '567cf9',
                '86a4fe',
                'b4c5fd',
                'c6c2f5',
                'e6c8e4',
                'f2b9b2',
                'd6a5d4',
                'ea8c80',
                'c881c5',
                'ea8c80',
                'c881c5',
                'e25f4d',
                'b968b5',
                'db3c27',
                '984c94',
                'b71c0a',
                '7f497d',
                '922016',
                '664766',
                '464148',
                '2c2d99',
                '312db5',
                '3d33ca',
                '6458e2',
                'a49bee'
            ], ar2 = [], i = start, ctr = 0;

            do {
                i = (i + 1) % ar.length;
                ctr++;
                ar2.push(ar[i]);
            } while (ctr < count);

            return ar2;
        }

        //#region sponsored category
        vm.sponsoredCategories = [
            {
                icon: 'face',
                id: 1,
                name: 'Employee of the year'
            }, {
                icon: 'favorite',
                id: 2,
                name: 'Employer of the year'
            }, {
                icon: 'settings_input_svideo',
                id: 3,
                name: 'Director of the Year'
            }
        ];
        //#endregion sponsored category

    }
})();
