(function () {
    'use strict';

    angular
        .module('app.dashboard')
        .controller('DashboardController', DashboardController);

    /* @ngInject */
    function DashboardController($q,
        userService,
        logger, $lodash,
        $state, $scope, $timeout, $window) {
        var vm = this;

        vm.tabs = [{ name: 'dashboard', state: 'dashboard.widgets' }, { name: 'timeline', state: 'dashboard.timeline' }];

        activate();

        function activate() {
            var promises = [
                    userService.getProfile(true)
            ];
            $q.all(promises).then(function (results) {

                vm.user = results[0].data;

                logger.info('Dashboard Activated');
            }).finally(function () {
            });

            $lodash.forEach(vm.tabs, function (n, key) {
                if ($state.current.name === n.state) {
                    vm.tabIndex = key;
                }
            });
        }

    }
})();
