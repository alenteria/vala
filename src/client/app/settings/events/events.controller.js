(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsEventsController', SettingsEventsController);

    /* @ngInject */
    function SettingsEventsController($q, $scope, logger, $state, $stateParams) {
        var vm = this;
        vm.isDetailpage = false;
        vm.eventId = 0;
        vm.tabs = ['settings.events.list', 'settings.events.calendar', 'settings.events.detail'];

        activate();

        function activate() {
            updateTabs();

            $scope.$watch(function () {
                return $state.current.name;
            }, function () {
                updateTabs();
            });
        }

        function updateTabs() {
            vm.isDetailpage = $state.current.name === 'settings.events.detail';
            vm.tabIndex = vm.tabs.indexOf($state.current.name);
        }
    }
})();
