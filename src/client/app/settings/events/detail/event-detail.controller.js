(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsEventDetailController', SettingsEventDetailController);

    /* @ngInject */
    function SettingsEventDetailController(
        $mdConstant,
        $mdpTimePicker,
        $q,
        $scope,
        $state,
        $stateParams,
        eventService,
        logger,
        roleService) {

        var vm = this;
        vm.event = { postcodes: [] };
        vm.stepPercentage = 20;
        vm.tabIndex = 0;
        vm.tabCount = 5;
        vm.eventId = $stateParams.id;
        this.keys = [$mdConstant.KEY_CODE.ENTER, $mdConstant.KEY_CODE.COMMA, $mdConstant.KEY_CODE.SPACE];

        vm.eventTypes = [
            { id: 1, name: 'By Organisation' },
            { id: 2, name: 'by County' },
            { id: 3, name: 'By Postcodes' },
            { id: 4, name: 'By City' }
        ];

        vm.next = next;
        vm.prev = prev;
        vm.getStatus = getStatus;
        vm.showStartTimePicker = showStartTimePicker;
        vm.showEndTimePicker = showEndTimePicker;

        //future improvements date range filter (min / max)

        activate();

        function activate() {
            var promises = [eventService.getById($stateParams.id)];
            $q.all(promises).then(function (responses) {
                if (responses[0].status === 200) {
                    vm.event = responses[0].data;
                    if (!vm.event.postcodes) {
                        vm.event.postcodes = [];
                    }
                } else {
                    logger.warn();
                }
            });
        }

        function getStatus() {
            var now = new Date();
            if (now > vm.event.start || now < vm.event.end) {
                return 'On-going';
            } else if (now > vm.event.end) {
                return 'Finished';
            } else if (now < vm.event.start) {
                return 'Upcomming';
            } else {
                return 'None';
            }
        }

        function showStartTimePicker($event) {
            $mdpTimePicker($event, vm.event.start).then(function (selectedDate) {
                vm.event.start = selectedDate;
            });
        }

        function showEndTimePicker($event) {
            $mdpTimePicker($event, vm.event.end).then(function (selectedDate) {
                vm.event.end = selectedDate;
            });
        }

        function next() {
            if (vm.tabIndex === vm.tabCount - 1) {
                return;
            }
            vm.tabIndex++;
        }

        function prev() {
            if (vm.tabIndex === 0) {
                return;
            }
            vm.tabIndex = vm.tabIndex - 1;
        }
    }
})();
