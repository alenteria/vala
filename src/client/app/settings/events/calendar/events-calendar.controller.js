(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsEventsCalendarController', SettingsEventsCalendarController);

    /* @ngInject */
    function SettingsEventsCalendarController($q, eventService, logger, $state, $stateParams) {
        var vm = this;
        vm.events = [];
        //#region pagination
        vm.limit = 10;
        vm.page = 1;
        //#endregion

        activate();

        function activate() {
            var promise = [eventService.query()];
            $q.all(promise).then(function (responses) {
                vm.events = responses[0].data;
                console.log(vm.events);
            });
        }

        vm.mobileClick = function mobileClick(item) {
            $state.go('settings.events.detail', { id: item.id });
        };

    }
})();
