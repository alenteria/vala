(function() {
    'use strict';

    angular.module('app.settings.events', [
        'app.core',
        'app.widgets',
        'mdPickers',
      ]);
})();
