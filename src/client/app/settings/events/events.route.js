(function () {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.events',
                config: {
                    abstract: true,
                    url: '/events',
                    templateUrl: 'app/settings/events/events.html',
                    controller: 'SettingsEventsController',
                    controllerAs: 'vm',
                    title: 'Events',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Events'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.events.list',
                config: {
                    url: '/list',
                    templateUrl: 'app/settings/events/list/events-list.html',
                    controller: 'SettingsEventsListController',
                    controllerAs: 'vm',
                    title: 'Events List',
                    settings: {
                        parent: 'settings.menu',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Settings List'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.events.calendar',
                config: {
                    url: '/calendar',
                    templateUrl: 'app/settings/events/calendar/events-calendar.html',
                    controller: 'SettingsEventsCalendarController',
                    controllerAs: 'vm',
                    title: 'Events Calendar',
                    settings: {
                        parent: 'settings',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Events Calendar'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.events.detail',
                config: {
                    url: '/:id',
                    templateUrl: 'app/settings/events/detail/event-detail.html',
                    controller: 'SettingsEventDetailController',
                    controllerAs: 'vm',
                    title: 'Event Detail',
                    settings: {
                        parent: 'settings.events.list',
                        nav: 1,
                        content: '<i class="fa fa-users"></i> Role Settings'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
