(function () {
    'use strict';

    angular
        .module('app.settings.categories')
        .controller('SettingsCategoryQuestionsListController', SettingsCategoryQuestionsListController);

    /* @ngInject */

    function SettingsCategoryQuestionsListController(
            $lodash,
            $mdDialog,
            $mdMedia,
            $q,
            $scope,
            $state,
            $stateParams,
            categoryEligibilityQuestionsService,
            questionService,
            choiceService,
            logger
            ) {

        var vm = this;

        //#question models

        vm.category = {id: $stateParams.id};
        vm.selectedQuestions = [];
        vm.questions = [];
        vm.query = { order: 'name', page: 1, limit: 10 };

        //#question functions

        vm.addEdit = addEdit;
        vm.bulkDeleteQuestions = bulkDeleteQuestions;

        activate();

        vm.mobileClick = function mobileClick(item, $event) {
            addEdit(item, $event);
        };

        //#question implementaions
        $scope.$watch('vm.questions', function(items) {
            angular.forEach(items, function(item) {
                questionService.getById(item.questionid).then(function(response) {
                    item.question = response.data.question;
                    choiceService.query({questionid: item.questionid}).then(function(response) {
                        item.question.choices = response.data.choices;
                    });
                });
            });
        });

        function activate() {
            var promise = [categoryEligibilityQuestionsService.query()];
            $scope.$emit('progress.show');
            categoryEligibilityQuestionsService.query({categoryid: parseInt(vm.category.id)}).then(function(responses) {
                vm.questions = responses.data.eligibilityQuestions;
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        function bulkDeleteQuestions($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected questions?')
               .textContent([vm.selectedQuestions.length,
                   ' Selected questions will be removed.'].join(''))
               .ariaLabel('Delete Questions')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                categoryEligibilityQuestionsService.bulkRemove(vm.selectedQuestions).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedQuestions, function (item) {
                            vm.questions.splice($lodash.findIndex(vm.questions, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' Question' : ' Questions',
                            ' deleted'].join(''));
                        vm.selectedQuestions = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }
        function addEdit(item, $event) {
            var addQuestionPopupOptions = {
                locals: {
                    categoryQuestion: item,
                    categoryQuestions: vm.questions
                },
                controller: 'SettingsCategoryQuestionModalController as vm',
                templateUrl: 'app/settings/categories/questions/list/category-question-modal.html',
                targetEvent: $event,
                clickOutsideToClose: true,
            };
            $mdDialog.show(addQuestionPopupOptions).then(function (question) {

                if (!item.id) {
                    //add mode
                    vm.questions.push(question);
                } else {
                    //edit mode
                    var updateQuestion = $lodash.find(vm.questions, { id: parseInt(question.id) });
                    updateQuestion.name = question.name;
                    updateQuestion.question = question.question;
                    updateQuestion.status = question.status;
                }

                logger.success(['Question ', question.name, (!item.id ? ' Added' : ' Updated')].join(''));
            });
        }

    }
})();
