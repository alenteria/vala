(function () {
    'use strict';

    angular
        .module('app.settings.questions')
        .controller('SettingsCategoryQuestionModalController', SettingsCategoryQuestionModalController);

    /* @ngInject */
    function SettingsCategoryQuestionModalController(
        $mdDialog,
        $lodash,
        $q,
        $scope,
        $stateParams,
        $timeout,
        questionService,
        categoryEligibilityQuestionsService,
        choiceService,
        categoryQuestion,
        categoryQuestions,
        logger) {

        var initializing = true;
        var vm = this;
        vm.categoryQuestion = angular.copy(categoryQuestion);
        if (vm.categoryQuestion.question == null) {
            vm.categoryQuestion.question = {id: 0, type: 'specific'};
        }

        vm.searchText = '';
        vm.hide = hide;
        vm.save = save;
        vm.loadQuestions = loadQuestions;
        vm.loadChoices = loadChoices;

        vm.addChoice = function() {
            vm.newChoice = {id: 0};
        };

        vm.saveChoice = function(question) {
            var newChoice = angular.copy(vm.newChoice);
            newChoice.id = new Date();
            question.choices.push(newChoice);
            vm.newChoice = null;
        };

        vm.questions = [];

        vm.question_types = [
            'open_ended_question',
            'multiple_choice',
            'number_range',
        ];

        vm.types = [
            'generic',
            'specific',
            'personal_bio_question',
            'organization_bio_question'
        ];

        vm.order_numbers = function() {
            var length_ = categoryQuestions.length;
            if (!vm.categoryQuestion.id) {
                length_ += 1;
            }
            return new Array(length_);
        };

        activate();

        $scope.$watch('vm.categoryQuestion.question.type', function(type) {
            if (initializing) {
                $timeout(function() { initializing = false; });
            } else {
                vm.categoryQuestion.question = {type: type};
            }
        });

        $scope.$watch('vm.categoryQuestion.question', function(id) {
            vm.categoryQuestion.questionid = (vm.questions.splice($lodash.findIndex(vm.questions, 'id', id), 1) || {}).id;
            vm.loadChoices(vm.categoryQuestion.question);
        });

        function activate() {
            vm.category = {id: $stateParams.id};
        }

        function hide() {
            $mdDialog.cancel();
        }

        function save() {
            //validation must occur here
            $scope.$emit('progress.show');
            if (vm.categoryQuestion.question.type === 'specific') {
                vm.categoryQuestion.question.categoryid = vm.category.id;
                questionService.save(vm.categoryQuestion.question).then(function (response) {
                    //expected result to be the newly added question with the id from api
                    if (response.status === 200) {
                        if (vm.categoryQuestion.id) {
                            vm.categoryQuestion.question = response.data.question;
                            categoryQuestions.splice($lodash.findIndex(categoryQuestions, 'id', vm.categoryQuestion.id), 1);
                            categoryQuestions.push(vm.categoryQuestion);
                        } else {
                            categoryEligibilityQuestionsService.save(vm.categoryQuestion).then(function(response2) {
                                vm.categoryQuestion = response2.data.eligibilityQuestion;
                                vm.categoryQuestion.question = response.data.question;
                                categoryQuestions.push(vm.categoryQuestion);
                            });
                        }

                        $mdDialog.hide(response.data.question);
                    } else {
                        $scope.questionForm.question.$setValidity('duplicate', false);
                    }
                }).finally(function () {

                });
            }else {
                categoryEligibilityQuestionsService.save(vm.categoryQuestion).then(function(response) {
                    if (!vm.categoryQuestion.id) {
                        categoryQuestions.push(vm.categoryQuestion);
                    }
                    else {
                        categoryQuestions.splice($lodash.findIndex(categoryQuestions, 'id', vm.categoryQuestion.id), 1);
                        categoryQuestions.push(vm.categoryQuestion);
                    }
                });
            }
            hide();
            $scope.$emit('progress.hide');

        }

        function loadQuestions(filter) {
            if (!vm.types.includes(filter.type)) {
                filter.type = 'specific';
            }
            filter.status = 'enabled';

            questionService.query(filter).then(function (response) {
                vm.questions = response.data.questions;
            });
        }

        function loadChoices(question) {
            var filter = {};//{questionid: question.id};
            choiceService.query(filter).then(function (response) {
                question.choices = response.data.choices;
            });
        }

    }
})();
