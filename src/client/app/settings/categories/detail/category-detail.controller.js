(function () {
    'use strict';

    angular
        .module('app.settings.categories')
        .controller('SettingsCategoryDetailController', SettingsCategoryDetailController);

    /* @ngInject */
    function SettingsCategoryDetailController(
        $mdDialog,
        $q,
        $scope,
        $state,
        $stateParams,
        categoryService,
        logger) {

        var vm = this;
        vm.searchText = '';
        vm.hide = hide;
        vm.save = save;
        vm.loadCategories = loadCategories;

        activate();

        function activate() {

            var promises = [loadCategories()];
            $scope.$emit('progress.show');
            $q.all(promises).then(function (responses) {
            }, function () {
                logger.warning('Opps, something went wrong!');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });

        }

        function hide() {
            $mdDialog.cancel();
        }

        function save() {
            //validation must occur here
            $scope.$emit('progress.show');
            categoryService.save(vm.category).then(function (response) {
                if (response.status === 200) {
                    logger.info('Category Saved!');
                    if (!parseInt($stateParams.id)) {
                        $state.go('settings.categories.detail', { id: response.data.category.id });
                    }
                } else {
                    $scope.categoryForm.category.$setValidity('duplicate', false);
                }
            }).finally(function () {
                //allways hide progress success or fail
                $scope.$emit('progress.hide');
            });
        }

        function loadCategories() {
            return categoryService.getById(parseInt($stateParams.id)).then(function (response) {
                vm.category = response.data.category;
            });
        }

    }
})();
