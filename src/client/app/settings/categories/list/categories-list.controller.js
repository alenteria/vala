(function () {
    'use strict';

    angular
        .module('app.settings.categories')
        .controller('SettingsCategoriesListController', SettingsCategoriesListController);

    /* @ngInject */
    function SettingsCategoriesListController(
        $lodash,
        $mdDialog,
        $mdMedia,
        $q,
        $scope,
        $state,
        $stateParams,
        categoryService,
        logger
        ) {

        var vm = this;

        //#category models

        vm.selectedCategories = [];
        vm.categories = [];
        vm.query = { order: 'name', page: 1, limit: 10 };

        //#category functions

        vm.addEdit = addEdit;
        vm.bulkDeleteCategories = bulkDeleteCategories;

        activate();

        vm.mobileClick = function mobileClick(item, $event) {
            addEdit(item, $event);
        };

        //#category implementaions

        function activate() {
            var promise = [categoryService.query()];
            $scope.$emit('progress.show');
            $q.all(promise).then(function (responses) {
                vm.categories = responses[0].data.categories;
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        function bulkDeleteCategories($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Categories?')
               .textContent([vm.selectedCategories.length,
                   ' Selected Categories will be removed.'].join(''))
               .ariaLabel('Delete Categories')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                categoryService.bulkRemove(vm.selectedCategories).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedCategories, function (item) {
                            vm.categories.splice($lodash.findIndex(vm.categories, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' Category' : ' Categories',
                            ' deleted'].join(''));
                        vm.selectedCategories = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }
        function addEdit(item, $event) {
            return $state.go('settings.categories.detail', { id: item.id });
        }

    }
})();
