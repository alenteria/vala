(function () {
    'use strict';

    angular
        .module('app.settings.categories')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.categories',
                config: {
                    abstract: true,
                    url: '/categories',
                    templateUrl: 'app/settings/categories/categories.html',
                    controller: 'SettingsCategoriesController',
                    controllerAs: 'vm',
                    title: 'Categories',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Categories'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.categories.list',
                config: {
                    url: '/list',
                    templateUrl: 'app/settings/categories/list/categories-list.html',
                    controller: 'SettingsCategoriesListController',
                    controllerAs: 'vm',
                    title: 'Categories',
                    settings: {
                        parent: 'settings.menu',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Categories'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.categories.detail',
                config: {
                    url: '/:id',
                    templateUrl: 'app/settings/categories/detail/category-detail.html',
                    controller: 'SettingsCategoryDetailController',
                    controllerAs: 'vm',
                    title: 'Category Detail',
                    settings: {
                        parent: 'settings.categories.list',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Category Detail'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.categories.questions',
                config: {
                    url: '/:id/questions',
                    templateUrl: 'app/settings/categories/questions/list/category-questions-list.html',
                    controller: 'SettingsCategoryQuestionsListController',
                    controllerAs: 'vm',
                    title: 'Category Questions',
                    settings: {
                        parent: 'settings.categories.list',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Questions'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
        ];
    }
})();
