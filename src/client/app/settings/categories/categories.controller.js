(function () {
    'use strict';

    angular
        .module('app.settings.categories')
        .controller('SettingsCategoriesController', SettingsCategoriesController);

    /* @ngInject */
    function SettingsCategoriesController($q, $rootScope, $scope, logger, $state, $stateParams) {
        var vm = this;

        vm.iAmAbtract = true;

        activate();

        function activate() {
            //TODO:
            $rootScope.$on('$stateChangeStart',
                function (event, toState, toParams, fromState, fromParams) {
                    //TODO: in case of debugging
                    //console.log(fromState.name, toState.name);
                }
            );
        }
    }
})();
