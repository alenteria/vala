(function () {
    'use strict';

    angular.module('app.settings.categories', [
        'app.core',
        'app.widgets',
        'googlechart',
        'mdColorPicker',
    ]);
})();
