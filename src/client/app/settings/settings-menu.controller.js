(function () {
    'use strict';

    angular
        .module('app.settings')
        .controller('SettingsMenuController', SettingsMenuController);

    //note: Need refractor to filter menus properly
    /* @ngInject */
    function SettingsMenuController($q, logger, $lodash) {
        var vm = this;
        vm.filter = '';
        vm.searchToolbarVisible = false;
        vm.toggleSearchBox = toggleSearchBox;
        vm.showByTags = showByTags;

        activate();

        function activate() {
            logger.info('Settings Menu Activated');
        }

        function toggleSearchBox(val) {
            vm.searchToolbarVisible = val;
        }

        function showByTags(tags) {
            if (!vm.filter) {
                return true;
            } else {
                return $lodash.findIndex(tags, function (o) {
                    return o.indexOf(vm.filter.toLocaleLowerCase()) > -1;
                }) > -1;
            }
        }

    }
})();
