(function () {
    'use strict';

    angular
        .module('app.settings')
        .controller('SettingsController', SettingsController);

    /* @ngInject */
    function SettingsController($q, logger, $state) {
        var vm = this;
        vm.title = 'Settings';
        vm.user = {};
        vm.user.position = 'Founder of Venus Award';
        vm.user.about = 'There are many variations of passages of ' +
            'Lorem Ipsum available, but the majority' +
            'have suffered alteration in some form Ipsum available.';

        activate();

        function activate() {
            //logger.info('Settings Activated');
        }

    }
})();
