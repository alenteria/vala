(function () {
    'use strict';

    angular
        .module('app.settings.content')
        .controller('SettingsContentLoginController',
                    SettingsContentLoginController);

    /* @ngInject */
    function SettingsContentLoginController($q, logger, $state) {
        var vm = this;
        vm.tab = '';
        vm.faces = [1, 2, 3, 4, 5, 6, 7, 8];
        vm.regText = '<p>' +
                    'If you would like to make a nomination or ' +
                    'apply for an award, please register.' +
                    '<br>' +
                    'It\'s totally free and we wont share your ' +
                    'information with third parties.' +
                    '</p>';

        vm.showTab = showTab;
        vm.removeFace = removeFace;

        activate();

        function activate() {

        }

        function showTab(tab) {
            vm.tab = tab;
        }

        function removeFace(index) {
            for (var i = 0; i < vm.faces.length; i++) {
                if (index === vm.faces[i]) {
                    vm.faces.splice(i, 1);
                    logger.info('Removed Face ' + index);
                    break;
                }
            }
        }

    }
})();
