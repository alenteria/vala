(function () {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.content',
                config: {
                    abstract: true,
                    url: '/content',
                    templateUrl: 'app/settings/content/content.html',
                    controller: 'SettingsContentController',
                    controllerAs: 'vm',
                    title: 'Content Management',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Content Management'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.content.menu',
                config: {
                    url: '/menu',
                    templateUrl: 'app/settings/content/content-menu.html',
                    controller: 'SettingsContentMenuController',
                    controllerAs: 'vm',
                    title: 'Settings Menu',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Settings Menu'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.content.login',
                config: {
                    url: '/login',
                    templateUrl: 'app/settings/content/login/login.html',
                    controller: 'SettingsContentLoginController',
                    controllerAs: 'vm',
                    title: 'Content Management - Login',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Login'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
