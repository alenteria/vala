(function() {
    'use strict';

    angular.module('app.settings.content', [
        'app.core',
        'app.widgets',
        'textAngular',
        'ui.sortable'
      ]);
})();
