(function () {
    'use strict';

    angular
        .module('app.settings.content')
        .controller('SettingsContentDashboardButtonsController',
            SettingsContentDashboardButtonsController);

    /* @ngInject */
    function SettingsContentDashboardButtonsController($q, roleService, logger, $state, $scope, $lodash) {

        var vm = this;

        vm.roles = [];
        vm.role = 1;
        vm.buttons = [];
        vm.selectedButtons = [];

        $scope.$watch('vm.role', function (val) {
            $lodash.forEach(vm.buttons, function (n, key) {
                if (parseInt(val) === n.roleId) {
                    console.log(n, 'selected');
                    vm.selectedButtons = n;
                    return;
                }
            });
        });

        activate();

        function activate() {
            var promise = [roleService.query()];
            $q.all(promise).then(function (responses) {
                vm.roles = responses[0].data;
                $lodash.forEach(vm.roles, function (n, key) {
                    var btn = createButtons();
                    btn.roleId = n.id;
                    vm.buttons.push(btn);
                });
            });
        }

        function createButtons(roleId) {
            var btns = [
                { id: 1, name: 'Nominate Someone', classname: 'md-primary', icon: 'mdi-account-plus', link: '', enabled: true },
                {
                    id: 2, name: 'Apply for an Award', classname: 'md-primary md-hue-2',
                    icon: 'mdi-account-box-outline', link: '', enabled: true
                },
                { id: 3, name: 'Edit Organizations', classname: 'md-dark', icon: 'mdi-pencil', link: '', enabled: true },
                { id: 4, name: 'Sent Nominations', classname: 'md-accent', icon: 'mdi-send', link: '', enabled: true },
                { id: 5, name: 'Edit My Profile', classname: 'md-dark', icon: 'mdi-pencil', link: '', enabled: true },
                {
                    id: 6, name: 'My Applications', classname: 'md-warn md-hue-2',
                    icon: 'mdi-account-box', link: '', enabled: true
                },
                {
                    id: 7, name: 'Judging', classname: 'md-warn md-hue-2',
                    icon: 'mdi-account-multiple', link: '', enabled: true
                },
                { id: 8, name: 'Received Nominations', classname: 'md-warn', icon: 'mdi-inbox', link: '', enabled: true }
            ];
            return btns;

        }

    }
})();
