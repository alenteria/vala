(function () {
    'use strict';

    angular
        .module('app.settings.content')
        .controller('SettingsContentDashboardWidgetController',
            SettingsContentDashboardWidgetController);

    /* @ngInject */
    function SettingsContentDashboardWidgetController($q, roleService, logger, $state, $scope, $lodash) {
        var vm = this;

        vm.roles = [];
        vm.role = 1;
        vm.chartrole = 1;
        vm.widgets = [];
        vm.charts = [];
        vm.selectedWidgets = [];
        vm.selectedCharts = [];

        $scope.$watch('vm.role', function (val) {
            $lodash.forEach(vm.widgets, function (n, key) {
                if (parseInt(val) === n.roleId) {
                    console.log(n, 'selected');
                    vm.selectedWidgets = n;
                    return;
                }
            });
        });

        $scope.$watch('vm.chartrole', function (val) {
            $lodash.forEach(vm.charts, function (n, key) {
                if (parseInt(val) === n.roleId) {
                    console.log(n, 'selected');
                    vm.selectedCharts = n;
                    return;
                }
            });
        });

        activate();

        function activate() {
            var promise = [roleService.query()];
            $q.all(promise).then(function (responses) {
                vm.roles = responses[0].data;
                $lodash.forEach(vm.roles, function (n, key) {
                    var btn = createWidgets(), crt = createCharts();
                    btn.roleId = n.id;
                    vm.widgets.push(btn);
                    crt.roleId = n.id;
                    vm.charts.push(crt);
                });
            });
        }

        function createWidgets(roleId) {
            var btns = [
                { id: 1, name: 'Nominations and Nominees', icon: 'mdi-chart-bar', link: '', enabled: true },
                { id: 2, name: 'Applications and Applicants', icon: 'mdi-chart-bar', link: '', enabled: true },
                { id: 3, name: 'Ticket Sales', icon: 'mdi-chart-areaspline', link: '', enabled: true },
                { id: 4, name: 'Sponsors', icon: 'mdi-chart-pie', link: '', enabled: true },
                { id: 5, name: 'Buttons', icon: 'mdi-apps', link: '', enabled: true },
                { id: 6, name: 'News Feed', icon: 'mdi-newspaper', link: '', enabled: true },
                { id: 7, name: 'News and Events', icon: 'mdi-calendar-text', link: '', enabled: true },
                { id: 8, name: 'Available Tickets', icon: 'mdi-ticket', link: '', enabled: true },
                { id: 9, name: 'Task List', icon: 'mdi-format-list-bulleted', link: '', enabled: true },
                { id: 10, name: 'Registered Users', icon: 'mdi-account-multiple-outline', link: '', enabled: true },
                { id: 11, name: 'Sponsored Categories', icon: 'mdi-format-list-bulleted-type', link: '', enabled: true },
                { id: 12, name: 'Events Timeline', icon: 'mdi-timetable', link: '', enabled: true },
            ];
            return btns;

        }

        function createCharts(roleId) {
            var crts = [
                { id: 1, name: 'Nominations and Nominees', icon: 'mdi-chart-bar', link: '', enabled: true, default: true },
                { id: 2, name: 'Applications and Applicants', icon: 'mdi-chart-bar', link: '', enabled: true, default: false },
                { id: 3, name: 'Ticket Sales', icon: 'mdi-chart-areaspline', link: '', enabled: true, default: false },
                { id: 4, name: 'Sponsors', icon: 'mdi-chart-pie', link: '', enabled: true, default: false },
                { id: 5, name: 'Events Timeline', icon: 'mdi-timetable', link: '', enabled: true, default: false },
            ];
            return crts;

        }

    }
})();
