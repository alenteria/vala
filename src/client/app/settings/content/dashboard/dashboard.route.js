(function () {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.content.dashboard',
                config: {
                    abstract: true,
                    url: '/dashboard',
                    templateUrl: 'app/settings/content/dashboard/dashboard.html',
                    controller: 'SettingsContentDashboardController',
                    controllerAs: 'vm',
                    title: 'Dashboard Content Management',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Dashboard Management'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.content.dashboard.content',
                config: {
                    url: '/content',
                    templateUrl: 'app/settings/content/dashboard/tabs/content.html',
                    controller: 'SettingsContentDashboardContentsController',
                    controllerAs: 'vm',
                    title: 'Content Management - Dashboard - Content',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Dashboard'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.content.dashboard.widgets',
                config: {
                    url: '/widgets',
                    templateUrl: 'app/settings/content/dashboard/tabs/widgets.html',
                    controller: 'SettingsContentDashboardWidgetController',
                    controllerAs: 'vm',
                    title: 'Content Management - Dashboard - Widgets',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Dashboard'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.content.dashboard.buttons',
                config: {
                    url: '/buttons',
                    templateUrl: 'app/settings/content/dashboard/tabs/buttons.html',
                    controller: 'SettingsContentDashboardButtonsController',
                    controllerAs: 'vm',
                    title: 'Content Management - Dashboard - Buttons',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Dashboard'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
