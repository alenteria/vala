(function () {
    'use strict';

    angular
        .module('app.settings.content')
        .controller('SettingsContentDashboardController',
                    SettingsContentDashboardController);

    /* @ngInject */
    function SettingsContentDashboardController($q, logger, $state, $lodash) {
        var vm = this;
        vm.showTab = showTab;
        vm.tabs = [
            { id: 1, name: 'Content', state: 'settings.content.dashboard.content' },
            { id: 2, name: 'Widgets', state: 'settings.content.dashboard.widgets' },
            { id: 3, name: 'Buttons', state: 'settings.content.dashboard.buttons' }
        ];

        activate();

        function activate() {
            $lodash.forEach(vm.tabs, function (n, key) {
                if ($state.current.name === n.state) {
                    vm.tabIndex = key;
                }
            });
        }

        function showTab(tab) {
            $state.transitionTo(tab.state);
        }

    }
})();
