(function () {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.roles',
                config: {
                    abstract: true,
                    url: '/roles',
                    templateUrl: 'app/settings/roles/roles.html',
                    controller: function () { },
                    controllerAs: 'vm',
                    title: 'Roles',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Settings'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.roles.list',
                config: {
                    url: '/list',
                    templateUrl: 'app/settings/roles/list/roles-list.html',
                    controller: 'SettingsRolesListController',
                    controllerAs: 'vm',
                    title: 'Role List',
                    settings: {
                        parent: 'settings.menu',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Settings List'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.roles.detail',
                config: {
                    url: '/:id',
                    templateUrl: 'app/settings/roles/detail/role-index.html',
                    controller: 'SettingsRolesSettingsController',
                    controllerAs: 'vm',
                    title: 'Role Detail',
                    settings: {
                        parent: 'settings.roles.list',
                        nav: 1,
                        content: '<i class="fa fa-users"></i> Role Settings'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
