(function() {
    'use strict';

    angular.module('app.settings.roles', [
        'app.core',
        'app.widgets',
        'textAngular',
      ]);
})();
