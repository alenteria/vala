(function () {
    'use strict';

    angular
        .module('app.settings.roles')
        .controller('SettingsRolesListController', SettingsRolesListController);

    /* @ngInject */
    function SettingsRolesListController(
            $lodash,
            $mdDialog,
            $q,
            $scope,
            roleService,
            logger,
            $state) {
        var vm = this;
        //#region models
        vm.selectedRoles = [];
        vm.roles = [];
        //#endregion

        //#region pagination
        vm.query = { order: 'rank', page: 1, limit: 10 };
        //#endregion

        //#region functions

        vm.bulkDelete = bulkDeleteRoles;
        vm.mobileClick = mobileClick;

        //#endregion

        activate();

        //#region implementations
        function activate() {
            var promise = [roleService.query()];
            $q.all(promise).then(function (responses) {
                vm.roles = responses[0].data.roles;
            });
        }

        function bulkDeleteRoles($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Roles?')
               .textContent([vm.selectedRoles.length,
                   ' Selected Roles will be removed.'].join(''))
               .ariaLabel('Delete Roles')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                roleService.bulkRemove(vm.selectedRoles).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedRoles, function (item) {
                            vm.roles.splice($lodash.findIndex(vm.roles, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' Role' : ' Roles',
                            ' deleted'].join(''));
                        vm.selectedRoles = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function mobileClick(item) {
            $state.go('settings.roles.detail', { id: item.id });
        }

        //#endregion

    }
})();
