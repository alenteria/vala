(function () {
    'use strict';

    angular
        .module('app.settings.roles')
        .controller('SettingsRolesSettingsController', SettingsRolesSettingsController);

    /* @ngInject */
    function SettingsRolesSettingsController(
            $lodash,
            $mdDialog,
            $q,
            $scope,
            $state,
            $stateParams,
            logger,
            roleService,
            userService,
            userRoleService
            ) {
        var vm = this;

        //#region models

        vm.active = 1;
        vm.type = 1;
        vm.name = 'Super Admin';
        vm.role = {};
        vm.selectedUsers = [];
        vm.users = [];
        vm.mobileClick = mobileClick;
        vm.description = 'Can access everything';
        vm.selectedModules = [];
        vm.modules = [
            {
                id: 1, slug: 'dashboard', name: 'Dashboard', value: 3, rank: 3,
                description: 'Dashboard Access (View)'
            },
            {
                id: 2, slug: 'dashboardmanagement', name: 'Dashboard Management', value: 2, rank: 2,
                description: 'Dashboard Management Page (Edit, View)'
            },
            {
                id: 3, slug: 'usermanagement', name: 'User Management', value: 1, rank: 1,
                desciption: 'User Management (Add, Edit, Delete, View)'
            },
            {
                id: 4, slug: 'rolemanagement', name: 'Role Management', value: 1, rank: 1,
                description: 'Role Mangement (Add, Edit, Delete, View)'
            }
        ];

        //#endregion

        //#region pagination

        vm.query = {};
        vm.query.users = { order: 'displayName', page: 1, limit: 10 };
        vm.query.modules = { order: 'rank', page: 1, limit: 10 };

        //#endregion

        //#region functions

        vm.setSlug = setSlug;
        vm.saveRole = saveRole;
        vm.bulkDeleteRoleUsers = bulkDeleteRoleUsers;

        //#endregion

        activate();

        function activate() {
            loadData();
        }

        function bulkDeleteRoleUsers($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to remove selected Users from this Role?')
               .textContent([vm.selectedUsers.length,
                   ' Selected Users will be removed from ', vm.role.name, ' role.'].join(''))
               .ariaLabel('Remove Users')
               .targetEvent($event)
               .ok('Remove Users')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                userRoleService.bulkRemove(vm.selectedUsers).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedUsers, function (item) {
                            vm.users.splice($lodash.findIndex(vm.roles, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' User' : ' Users',
                            ' Removed from the Role ', vm.role.name].join(''));
                        vm.selectedUsers = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function loadData() {
            $scope.$emit('progress.show');
            var promise = [
                    userRoleService.query({ roleid: parseInt($stateParams.id) }),
                    roleService.getById(parseInt($stateParams.id))
            ];
            $q.all(promise).then(function (responses) {
                if (responses[0].status === 200) {
                    vm.users = responses[0].data.userroles;
                } else {
                    logger.warning('Unable to load role users...');
                }
                if (responses[1].status === 200) {
                    vm.role = responses[1].data.role;
                } else {
                    logger.warning('Unable to load role details...');
                }
            }, function () {
                logger.warning('Unable to load page data...');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        function setSlug() {
            if (!vm.role.name) {
                vm.role.system_name = '';
            } else {
                vm.role.system_name = vm.role.name.replace(/[^a-zA-Z0-9]/gi, '').toLowerCase();
            }
        }

        function mobileClick(item) {
            $state.go('settings/users/view', { id: item.id });
        }

        function saveRole() {
            var response = roleService.save(vm.role);

            if (response.status === 200) {
                logger.success(response.message, response);
            } else {
                logger.warning(response.message);
            }
        }
    }
})();
