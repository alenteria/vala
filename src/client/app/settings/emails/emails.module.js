(function() {
    'use strict';

    angular.module('app.settings.emails', [
        'app.core',
        'app.widgets',
        'textAngular',
        'ui.sortable'
      ]);
})();
