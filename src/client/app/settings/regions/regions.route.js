(function () {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.regions',
                config: {
                    abstract: true,
                    url: '/regions',
                    templateUrl: 'app/settings/regions/regions.html',
                    controller: 'SettingsRegionsController',
                    controllerAs: 'vm',
                    title: 'Regions',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Regions'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.regions.list',
                config: {
                    url: '/list',
                    templateUrl: 'app/settings/regions/list/regions-list.html',
                    controller: 'SettingsRegionsListController',
                    controllerAs: 'vm',
                    title: 'Regions Areas',
                    settings: {
                        parent: 'settings.menu',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Settings List'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.regions.detail',
                config: {
                    url: '/:id',
                    templateUrl: 'app/settings/regions/detail/region-detail.html',
                    controller: 'SettingsRegionDetailController',
                    controllerAs: 'vm',
                    title: 'Region Detail',
                    settings: {
                        parent: 'settings.regions.list',
                        nav: 1,
                        content: '<i class="fa fa-users"></i> Role Settings'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.regions.cities',
                config: {
                    url: '/:id/counties/:countyid/cities',
                    templateUrl: 'app/settings/regions/detail/city/city-list.html',
                    controller: 'SettingsRegionCityListController',
                    controllerAs: 'vm',
                    title: 'Cities',
                    settings: {
                        parent: 'settings.regions.detail',
                        nav: 1,
                        content: '<i class="fa fa-users"></i> Role Settings'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.regions.city',
                config: {
                    url: '/:id/counties/:countyid/cities/:cityid',
                    templateUrl: 'app/settings/regions/detail/city/city-detail.html',
                    controller: 'SettingsRegionCityDetailController',
                    controllerAs: 'vm',
                    title: 'City',
                    settings: {
                        parent: 'settings.regions.cities',
                        nav: 1,
                        content: '<i class="fa fa-users"></i> City Detail'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
