(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsRegionDetailCountyModalController', SettingsRegionDetailCountyModalController);

    /* @ngInject */
    function SettingsRegionDetailCountyModalController(
        $mdDialog,
        $q,
        $scope,
        $stateParams,
        regionService,
        countyService,
        county,
        logger) {

        var vm = this;
        vm.county = county;
        vm.oldCounty = angular.copy(county);
        vm.hide = hide;
        vm.save = save;

        activate();

        function activate() {

            //TODO: Some code if needed, for now passed county data to local scope

            //var promises = [countyService.getById(county.id)];

            //$q.all(promises).then(function (responses) {
            //    vm.county = responses[0].data.county;
            //});

        }

        function hide() {
            $mdDialog.cancel();
        }

        function save() {
            //check if modified
            if (vm.county.id && vm.oldCounty.name === vm.county.name) {
                //return cancel
                return vm.hide();
            }
            //validation must occur here
            $scope.$emit('progress.show');
            countyService.save(vm.county).then(function (response) {
                //expected result to be the newly added county with the id from api
                if (response.status === 200) {
                    $mdDialog.hide(response.data.county);
                } else {
                    $scope.countyForm.county.$setValidity('duplicate', false);
                }
            }).finally(function () {
                //allways hide progress success or fail
                $scope.$emit('progress.hide');
            });
        }

    }
})();
