(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsRegionCityDetailController', SettingsRegionCityDetailController);

    /* @ngInject */
    function SettingsRegionCityDetailController(
        $mdDialog,
        $q,
        $scope,
        $state,
        $stateParams,
        countyService,
        regionService,
        cityService,
        logger) {

        var vm = this;
        vm.city = {
            id: parseInt($stateParams.cityid),
            countyid: parseInt($stateParams.countyid) || null
        };
        vm.regionid = parseInt($stateParams.regionid);
        vm.countyid = parseInt($stateParams.countyid); //use as old countyid readonly
        vm.hide = hide;
        vm.save = save;
        vm.postcodes = [];

        activate();

        function activate() {

            if ($stateParams.id) {
                if (vm.city.id) {
                    loadData();
                }
            } else {
                $state.go('settings.regions.cities', $stateParams);
            }
        }

        function hide() {
            $mdDialog.cancel();
        }

        function loadData() {

            var promises = [cityService.getById(vm.city.id),
                            countyService.query({ regionid: vm.regionid })];
            $scope.$emit('progress.show');
            $q.all(promises).then(function (responses) {
                console.log(responses);
                vm.city = responses[0].data.city;
                vm.postcodes = vm.city.postcodes;
                vm.counties = responses[1].data.counties;
            }).finally(function () {
                $scope.$emit('progress.hide');
            });

        }

        function save() {
            //validation must occur here
            $scope.$emit('progress.show');
            cityService.save(vm.city).then(function (response) {
                //expected result to be the newly added city with the id from api
                if (response.status === 200) {
                    $state.go('settings.regions.cities', $stateParams);
                } else {
                    $scope.cityForm.city.$setValidity('duplicate', false);
                }
            }, function () {
                logger.warn('Opps, Cannot save city!');
            }).finally(function () {
                //allways hide progress success or fail
                $scope.$emit('progress.hide');
            });
        }

    }
})();
