(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsRegionCityListController', SettingsRegionCityListController);

    /* @ngInject */
    function SettingsRegionCityListController(
        $lodash,
        $mdDialog,
        $mdpTimePicker,
        $q,
        $rootScope,
        $scope,
        $state,
        $stateParams,
        regionService,
        cityService,
        countyService,
        logger,
        roleService) {

        var vm = this;

        //#region model declarations

        vm.tabs = ['cities'];
        vm.query = {
            cities: { order: 'name', page: 1, limit: 10 }
        };
        vm.filter = { search: '' };
        vm.cities = [];
        vm.selectedCities = [];
        vm.regionid = $stateParams.id;
        vm.countyid = $stateParams.countyid;

        //#endregion models declarations

        //#region functions declarations

        vm.mobileClick = mobileClick;
        vm.bulkDeleteCities = bulkDeleteCities;

        //#endregion functions declarations

        activate();

        //#region implementaions

        function activate() {

            if (parseInt(vm.regionid)) {
                //atleast regionid is required to list a city
                loadData();
            } else {
                //there's nothing to do here, go back to region list
                $state.go('settings.regions.list');
            }
        }

        function mobileClick(item, $event) {
            $state.go('settings.regions.city', {
                id: vm.regionid,
                countyid: vm.countyid,
                cityid: item.id
            });
        }

        function bulkDeleteCities($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Cities?')
               .textContent([vm.selectedCities.length,
                   ' Selected cities will be removed.'].join(''))
               .ariaLabel('Delete City')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                cityService.bulkRemove(vm.region.id, vm.selectedCities).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedCities, function (item) {
                            vm.cities.splice($lodash.findIndex(vm.cities, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' City' : ' Cities',
                            ' deleted'].join(''));
                        vm.selectedCities = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function loadData() {
            var promises = [cityService.query({
                regionid: $stateParams.id,
                countyid: $stateParams.countyid
            })];
            $scope.$emit('progress.show');
            //load required data
            $q.all(promises).then(function (responses) {
                if (responses[0].status === 200) {
                    vm.cities = responses[0].data.cities;
                }
            }, function () {
                $state.go('settings.regions.list');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        //#endregion implementaions
    }
})();
