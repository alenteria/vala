(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsRegionPostcodeListController', SettingsRegionPostcodeListController);

    /* @ngInject */
    function SettingsRegionPostcodeListController(
        $lodash,
        $mdDialog,
        $mdpTimePicker,
        $q,
        $rootScope,
        $scope,
        $state,
        $stateParams,
        regionService,
        cityService,
        countyService,
        logger,
        roleService) {

        var vm = this;

        //#region model declarations

        vm.tabs = ['cities'];
        vm.query = {
            cities: { order: 'name', page: 1, limit: 10 }
        };
        vm.filter = { search: '' };
        vm.region = { id: 0, name: '', status: 1 };
        vm.cities = [];
        vm.counties = null;
        vm.county = {};
        vm.selectedCities = [];
        vm.activeTabIndex = 0;

        //#endregion models declarations

        //#region functions declarations

        vm.save = save;
        vm.editPostcode = editPostcode;
        vm.bulkDeleteCities = bulkDeleteCities;

        //#endregion functions declarations

        activate();

        //#region implementaions

        function activate() {

            if (parseInt($stateParams.id)) {
                //load data when edit mode
                loadData();
            } else {
                //new region build
                vm.region.name = '';
                vm.region.id = 0;
                vm.region.status = 1;
            }
        }

        function editPostcode(item, $event) {
            var addPostcodePopupOptions = {
                locals: {
                    city: angular.copy(item) //do not use original model
                },
                controller: 'SettingsRegionDetailPostcodeModalController as vm',
                templateUrl: 'app/settings/regions/detail/city/city-modal.html',
                targetEvent: $event,
                clickOutsideToClose: true
            };
            $mdDialog.show(addPostcodePopupOptions).then(function (city) {

                if (!item.id) {
                    //add mode
                    vm.cities.push(city);
                } else {
                    //edit mode
                    var updatePostcode = $lodash.find(vm.cities, { id: parseInt(city.id) });
                    updatePostcode.name = city.name;
                }

                logger.success(['Postcode ', city.name, (!item.id ? ' Added' : ' Updated')].join(''));
            });
        }

        function bulkDeleteCities($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Cities?')
               .textContent([vm.selectedCities.length,
                   ' Selected cities will be removed.'].join(''))
               .ariaLabel('Delete Postcode')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                cityService.bulkRemove(vm.region.id, vm.selectedCities).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedCities, function (item) {
                            vm.cities.splice($lodash.findIndex(vm.cities, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' Postcode' : ' Cities',
                            ' deleted'].join(''));
                        vm.selectedCities = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function loadData() {
            var promises = [regionService.getById($stateParams.id),
                            cityService.query({
                                regionid: $stateParams.id,
                                countyid: $stateParams.countyid
                            }),
                            countyService.query({ regionid: $stateParams.id }),
                            countyService.getById($stateParams.countyid)];
            //load required data
            $q.all(promises).then(function (responses) {
                if (responses[0].status === 200) {
                    vm.region = responses[0].data.region;
                }
                if (responses[1].status === 200) {
                    vm.cities = responses[1].data.cities;
                }
                if (responses[2].status === 200) {
                    vm.counties = responses[2].data.counties;
                }
                if (responses[3].status === 200) {
                    vm.county = responses[3].data.county;
                }
                if (!vm.region || !vm.region.id) {
                    //redirect to list when ID not found
                    //for edit mode it should have a data and id
                    $state.go('settings.regions.list');
                }
            }, function () {
                $state.go('settings.regions.list');
            });
        }

        function save() {
            $scope.$emit('progress.show');
            regionService.save(vm.region).then(function (response) {
                if (response.status === 200) {
                    if (!vm.region.id) {
                        //add mode
                        vm.region = response.data.region;
                        logger.success(['Region ', response.data.region.name, ' has been added']);
                        //replace back to list when clicking back button from browser
                        $rootScope.$viewHistory.currentView = $rootScope.$viewHistory.backView;
                        $state.go('settings.regions.detail', { id: response.data.region.id });
                    } else {
                        //edit mode
                        logger.success(['Region ', response.data.region.name, ' has been updated'].join(''));
                    }
                } else if (response.status === 409) {
                    //improvements, make a directive
                    $scope.regionForm.name.$setValidity('duplicate', false);
                } else {
                    logger.warning(response.data.message);
                }
            }, function () {
                logger.warning('Unable to process request!');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        //#endregion implementaions
    }
})();
