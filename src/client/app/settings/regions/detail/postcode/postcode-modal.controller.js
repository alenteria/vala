(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsRegionDetailPostcodeModalController', SettingsRegionDetailPostcodeModalController);

    /* @ngInject */
    function SettingsRegionDetailPostcodeModalController(
        $mdDialog,
        $q,
        $scope,
        $stateParams,
        regionService,
        postcodeService,
        postcode,
        logger) {

        var vm = this;
        vm.postcode = postcode;
        vm.oldPostcode = angular.copy(postcode);
        vm.hide = hide;
        vm.save = save;

        activate();

        function activate() {

            //TODO: Some code if needed, for now passed postcode data to local scope

            //var promises = [postcodeService.getById(postcode.id)];

            //$q.all(promises).then(function (responses) {
            //    vm.postcode = responses[0].data.postcode;
            //});

        }

        function hide() {
            $mdDialog.cancel();
        }

        function save() {
            //check if modified
            if (vm.postcode.id && vm.oldPostcode.name === vm.postcode.name) {
                //return cancel
                return vm.hide();
            }
            //validation must occur here
            $scope.$emit('progress.show');
            postcodeService.save(vm.postcode).then(function (response) {
                //expected result to be the newly added postcode with the id from api
                if (response.status === 200) {
                    $mdDialog.hide(response.data.postcode);
                } else {
                    $scope.postcodeForm.postcode.$setValidity('duplicate', false);
                }
            }).finally(function () {
                //allways hide progress success or fail
                $scope.$emit('progress.hide');
            });
        }

    }
})();
