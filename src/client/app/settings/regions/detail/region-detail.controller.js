(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsRegionDetailController', SettingsRegionDetailController);

    /* @ngInject */
    function SettingsRegionDetailController(
        $lodash,
        $mdDialog,
        $mdpTimePicker,
        $q,
        $rootScope,
        $scope,
        $state,
        $stateParams,
        chartDataService,
        regionService,
        countyService,
        logger,
        roleService) {

        var vm = this;

        //#region model declarations

        vm.tabs = ['detail'];
        vm.query = {
            counties: { order: 'name', page: 1, limit: 10 },
            cities: { order: 'name', page: 1, limit: 10 },
            postcodes: { order: 'name', page: 1, limit: 10 }
        };
        vm.filter = { search: '' };
        vm.regionArea = { id: 0, name: '', status: 1 };
        vm.counties = [];
        vm.cities = [];
        vm.selectedCounties = [];
        vm.selectedCities = [];
        vm.regionData = [{ v: '' }, { v: 0 }];
        vm.activeTabIndex = 0;

        //#endregion models declarations

        //#region functions declarations

        vm.regionChartObject = buildChartOptions();
        vm.save = save;
        vm.editCounty = editCounty;
        vm.bulkDeleteCounties = bulkDeleteCounties;

        //#endregion functions declarations

        activate();

        //#region implementaions

        function activate() {

            if (parseInt($stateParams.id)) {
                //load data when edit mode
                loadData();
            } else {
                //new region build
                vm.regionArea.name = '';
                vm.regionArea.id = 0;
                vm.regionArea.status = 1;
            }

            //Watch regionData and Apply to Chart
            $scope.$watch(function () {
                return vm.regionArea ? vm.regionArea.name : null;
            }, function (newVal) {
                vm.regionData[0].v = newVal;
            });
            //watch counties length
            $scope.$watch(function () {
                return vm.counties.length;
            }, function (newVal) {
                vm.regionData[1].v = newVal;
            });

        }

        function editCounty(item, $event) {
            var addCountyPopupOptions = {
                locals: {
                    county: angular.copy(item) //do not use original model
                },
                controller: 'SettingsRegionDetailCountyModalController as vm',
                templateUrl: 'app/settings/regions/detail/county/county-modal.html',
                targetEvent: $event,
                clickOutsideToClose: true
            };
            $mdDialog.show(addCountyPopupOptions).then(function (county) {

                if (!item.id) {
                    //add mode
                    vm.counties.push(county);
                } else {
                    //edit mode
                    var updateCounty = $lodash.find(vm.counties, { id: parseInt(county.id) });
                    updateCounty.name = county.name;
                }

                logger.success(['County ', county.name, (!item.id ? ' Added' : ' Updated')].join(''));
            });
        }

        function bulkDeleteCounties($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Counties?')
               .textContent([vm.selectedCounties.length,
                   ' Selected counties will be removed.'].join(''))
               .ariaLabel('Delete County')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                countyService.bulkRemove(vm.regionArea.id, vm.selectedCounties).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedCounties, function (item) {
                            vm.counties.splice($lodash.findIndex(vm.counties, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' County' : ' Counties',
                            ' deleted'].join(''));
                        vm.selectedCounties = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function buildChartOptions() {
            var options = {
                type: 'GeoChart',
                data: {
                    'cols': [
                        { id: 'r', label: 'Regions', type: 'string' },
                        { id: 'a', label: 'Count', type: 'number' }
                    ], 'rows': [
                        {
                            c: vm.regionData
                        }
                    ]
                },
                options: {
                    enableRegionInteractivity: false,
                    region: 'GB',
                    displayMode: 'markers',
                }
            };
            return options;
        }

        function loadData() {
            var promises = [regionService.getById($stateParams.id)];
            //countyService.query({ regionid: $stateParams.id })];
            $scope.$emit('progress.show');
            //load required data
            $q.all(promises).then(function (responses) {
                if (responses[0].status === 200) {
                    vm.regionArea = responses[0].data.regionArea;
                    vm.regionData.v = vm.regionArea ? vm.regionArea.name : '';
                    vm.counties = vm.regionArea.counties;
                    vm.cities = vm.regionArea.cities;
                }
                if (!vm.regionArea || !vm.regionArea.id) {
                    //redirect to list when ID not found
                    //for edit mode it should have a data and id
                    $state.go('settings.regions.list');
                }
            }, function () {
                $state.go('settings.regions.list');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        function save() {
            $scope.$emit('progress.show');
            regionService.save(vm.regionArea).then(function (response) {
                if (response.status === 200) {
                    if (!vm.regionArea.id) {
                        //add mode
                        vm.regionArea = response.data.regionArea;
                        logger.success(['Region ', response.data.regionArea.name, ' has been added']);
                        //replace back to list when clicking back button from browser
                        $rootScope.$viewHistory.currentView = $rootScope.$viewHistory.backView;
                        $state.go('settings.regions.detail', { id: response.data.regionArea.id });
                    } else {
                        //edit mode
                        logger.success(['Region ', response.data.regionArea.name, ' has been updated'].join(''));
                    }
                } else if (response.status === 409) {
                    //improvements, make a directive
                    $scope.regionForm.name.$setValidity('duplicate', false);
                } else {
                    logger.warning(response.data.message);
                }
            }, function () {
                logger.warning('Unable to process request!');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        //#endregion implementaions
    }
})();
