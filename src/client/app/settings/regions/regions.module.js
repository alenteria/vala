(function() {
    'use strict';

    angular.module('app.settings.regions', [
        'app.core',
        'app.widgets',
        'googlechart',
        'mdColorPicker',
      ]);
})();
