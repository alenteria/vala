(function () {
    'use strict';

    angular
        .module('app.settings.events')
        .controller('SettingsRegionsListController', SettingsRegionsListController);

    /* @ngInject */
    function SettingsRegionsListController(
        $lodash,
        $mdDialog,
        $mdMedia,
        $q,
        $scope,
        $state,
        $stateParams,
        regionService,
        logger
        ) {
        var vm = this;

        //#region models

        vm.selectedRegions = [];
        vm.regionAreas = [];
        vm.query = { order: 'name', page: 1, limit: 10 };

        //#endregion

        //#region functions

        vm.bulkDeleteRegions = bulkDeleteRegions;

        //#endregion

        activate();

        //#region implementaions

        function activate() {
            var promise = [regionService.query()];
            $scope.$emit('progress.show');
            $q.all(promise).then(function (responses) {
                vm.regionAreas = responses[0].data.regionAreas;
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        function bulkDeleteRegions($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Regions?')
               .textContent([vm.selectedRegions.length,
                   ' Selected Regions will be removed.'].join(''))
               .ariaLabel('Delete Regions')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                regionService.bulkRemove(vm.selectedRegions).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedRegions, function (item) {
                            vm.regions.splice($lodash.findIndex(vm.regions, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' Region' : ' Regions',
                            ' deleted'].join(''));
                        vm.selectedRegions = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        vm.mobileClick = function mobileClick(item) {
            $state.go('settings.regions.detail', { id: item.id });
        };

        //#endregion
    }
})();
