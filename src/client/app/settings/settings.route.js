(function () {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings',
                config: {
                    abstract: true,
                    url: '/settings',
                    templateUrl: 'app/settings/settings.html',
                    controller: 'SettingsController',
                    controllerAs: 'vm',
                    title: 'Settings',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Settings'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.menu',
                config: {
                    url: '/menu',
                    templateUrl: 'app/settings/settings-menu.html',
                    controller: 'SettingsMenuController',
                    controllerAs: 'vm',
                    title: 'Settings',
                    settings: {
                        admin: {
                            level: 1
                        },
                        icon: 'mdi-settings'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings/emails',
                config: {
                    url: '/settings/emails',
                    templateUrl: 'app/settings/emails/emails.html',
                    controller: 'SettingsEmailController',
                    controllerAs: 'vm',
                    title: 'Emails',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Emails'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings/emails/reports',
                config: {
                    url: '/settings/emails/reports',
                    templateUrl: 'app/settings/emails/reports/reports.html',
                    controller: 'SettingsEmailsReportsController',
                    controllerAs: 'vm',
                    title: 'Emails - Reports',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Reports'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings/emails/templates',
                config: {
                    url: '/settings/emails/templates',
                    templateUrl: 'app/settings/emails/templates/templates.html',
                    controller: 'SettingsEmailsTemplatesController',
                    controllerAs: 'vm',
                    title: 'Emails - Templates',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Templates'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
