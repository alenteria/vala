(function () {
    'use strict';

    angular.module('app.settings', [
        'app.core',
        'app.widgets',
        'md.data.table',
        'app.settings.content',
        'app.settings.emails',
        'app.settings.events',
        'app.settings.categories',
        'app.settings.questions',
        'app.settings.regions',
        'app.settings.roles',
        'app.settings.users',
        'ngFileUpload',
    ]);
})();
