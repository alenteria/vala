(function () {
    'use strict';

    angular
        .module('app.settings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.users',
                config: {
                    abstract: true,
                    url: '/users',
                    templateUrl: 'app/settings/users/users.html',
                    controller: 'SettingsUsersController',
                    controllerAs: 'vm',
                    title: 'Users',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Users'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.users.list',
                config: {
                    url: '/list',
                    templateUrl: 'app/settings/users/list/users-list.html',
                    controller: 'SettingsUserListController',
                    controllerAs: 'vm',
                    title: 'User List',
                    settings: {
                        parent: 'settings.menu',
                        nav: 1,
                        content: '<i class="fa fa-users"></i> Users'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.users.detail',
                config: {
                    url: '/:id',
                    templateUrl: 'app/settings/users/detail/user-detail.html',
                    controller: 'SettingsUsersDetailController',
                    controllerAs: 'vm',
                    title: 'User Detail',
                    settings: {
                        parent: 'settings.users.list',
                        nav: 1,
                        content: '<i class="fa fa-users"></i> Users View'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
