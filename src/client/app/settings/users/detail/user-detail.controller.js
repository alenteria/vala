(function () {
    'use strict';

    angular
        .module('app.settings.users')
        .controller('SettingsUsersDetailController', SettingsUsersDetailController);

    /* @ngInject */
    function SettingsUsersDetailController($q, userService, organizationsService,
        logger, $state, $scope, Upload, $stateParams) {
        var vm = this;
        vm.active = 1;
        vm.type = 1;
        vm.roleName = 'Super Admin';
        vm.role = 1;
        vm.genders = [{ name: 'male' }, { name: 'female' }];
        vm.counties = [{ id: 1, name: 'Dorset' }, { id: 2, name: 'Devon' }];
        vm.user = {};
        vm.industrie = [];

        vm.uploadPicture = uploadPicture;
        vm.uploadOrgPicture = uploadOrgPicture;

        activate();

        function activate() {
            var promise = [userService.getUserById($stateParams.id), organizationsService.query()];
            $q.all(promise).then(function (responses) {
                vm.user = responses[0];
                vm.industries = responses[1].data;
            });
        }

        // upload on file select
        function uploadPicture(file) {
            userService.uploadPicture({ file: file, 'type': 'user' }).then(function (resp) {
                console.log('uploaded. Response: ' + resp.data);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        }
        // upload on file select
        function uploadOrgPicture(file) {
            organizationsService.uploadPicture({ file: file, 'type': 'org' }).then(function (resp) {
                console.log('uploaded. Response: ' + resp.data);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        }

    }
})();
