(function () {
    'use strict';

    angular
        .module('app.settings.users')
        .controller('SettingsUserListController', SettingsUserListController);

    /* @ngInject */
    function SettingsUserListController(
            $lodash,
            $mdDialog,
            $q,
            $scope,
            $state,
            $timeout,
            userService,
            logger
            ) {
        var vm = this;

        //#region model

        vm.selectedUsers = [];
        vm.users = [];

        //#endregion

        //#region pagination
        vm.query = { order: 'name', page: 1, limit: 10 };
        //#endregion

        //#region functions

        vm.bulkDelete = bulkDelete;
        vm.mobileClick = mobileClick;
        vm.onPaginate = onPaginate;

        //#endregion

        activate();

        //#region implementations

        function activate() {
            var promises = [userService.getUsers(vm.query), userService.getCount()];
            $q.all(promises).then(function (responses) {
                vm.users = responses[0].data.users;
                vm.userCount = responses[1].data.userCount;
            });
        }

        function onPaginate(page, limit) {
            vm.query.page = page;
            vm.query.limit = limit;

            //vm.selectedUsers = [];

            //vm.promise = $timeout(function() {
            userService.getUsers(vm.query).then(
                function (response) {
                    vm.users = response.data.users;
                }
            );
            //}, 2000);
        }

        function bulkDelete($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Users?')
               .textContent([vm.selectedUsers.length,
                   ' Selected Users will be permanently deleted from the database.'].join(''))
               .ariaLabel('Delete Users')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                userService.bulkRemove(vm.selectedUsers).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedUsers, function (item) {
                            vm.users.splice($lodash.findIndex(vm.users, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' User' : ' Users',
                            ' deleted'].join(''));
                        vm.selectedUsers = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function mobileClick(item) {
            $state.go('settings.users.detail', { id: item.id });
        }

        //#endregion

    }
})();
