(function() {
    'use strict';

    angular.module('app.settings.users', [
        'app.core',
        'app.widgets',
        'textAngular',
      ]);
})();
