(function () {
    'use strict';

    angular
        .module('app.settings.questions')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'settings.questions',
                config: {
                    abstract: true,
                    url: '/questions',
                    templateUrl: 'app/settings/questions/questions.html',
                    controller: 'SettingsQuestionsController',
                    controllerAs: 'vm',
                    title: 'Questions',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Questions'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.questions.list',
                config: {
                    url: '/list',
                    templateUrl: 'app/settings/questions/list/questions-list.html',
                    controller: 'SettingsQuestionsListController',
                    controllerAs: 'vm',
                    title: 'Questions',
                    settings: {
                        parent: 'settings.menu',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Questions'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'settings.questions.choices',
                config: {
                    url: '/:id/choices',
                    templateUrl: 'app/settings/questions/choices/choices-list.html',
                    controller: 'SettingsChoicesListController',
                    controllerAs: 'vm',
                    title: 'Choices',
                    settings: {
                        parent: 'settings.questions.list',
                        nav: 1,
                        content: '<i class="fa fa-gear"></i> Choices'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
