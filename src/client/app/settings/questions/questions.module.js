(function () {
    'use strict';

    angular.module('app.settings.questions', [
        'app.core',
        'app.widgets',
        'googlechart',
        'mdColorPicker',
    ]);
})();
