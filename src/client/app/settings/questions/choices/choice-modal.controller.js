(function () {
    'use strict';

    angular
        .module('app.settings.questions')
        .controller('SettingsChoiceModalController', SettingsChoiceModalController);

    /* @ngInject */
    function SettingsChoiceModalController(
        $mdDialog,
        $q,
        $scope,
        $stateParams,
        choiceService,
        choice,
        logger) {

        var vm = this;
        vm.choice = choice;
        vm.oldChoice = angular.copy(choice);
        vm.searchText = '';
        vm.hide = hide;
        vm.save = save;
        vm.loadChoices = loadChoices;
        vm.choices = [];

        activate();

        function activate() {

            var promises = [loadChoices()];
            $scope.$emit('progress.show');
            $q.all(promises).then(function (responses) {
            }, function () {
                logger.warning('Opps, something went wrong!');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });

        }

        function hide() {
            $mdDialog.cancel();
        }

        function save() {
            //validation must occur here
            $scope.$emit('progress.show');
            choiceService.save(vm.choice).then(function (response) {
                //expected result to be the newly added choice with the id from api
                if (response.status === 200) {
                    $mdDialog.hide(response.data.choice);
                } else {
                    $scope.choiceForm.choice.$setValidity('duplicate', false);
                }
            }).finally(function () {
                //allways hide progress success or fail
                $scope.$emit('progress.hide');
            });
        }

        function loadChoices() {
            return vm.choices.length || choiceService.query({ subchoice: false }).then(function (response) {
                vm.choices = response.data.choices;
            });
        }

    }
})();
