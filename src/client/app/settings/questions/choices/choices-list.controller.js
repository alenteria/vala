(function () {
    'use strict';

    angular
        .module('app.settings.questions')
        .controller('SettingsChoicesListController', SettingsChoicesListController);

    /* @ngInject */
    function SettingsChoicesListController(
        $lodash,
        $mdDialog,
        $mdMedia,
        $q,
        $scope,
        $state,
        $stateParams,
        choiceService,
        logger
        ) {
        var vm = this;

        //#choice models

        vm.selectedChoices = [];
        vm.choices = [];
        vm.query = { order: 'name', page: 1, limit: 10 };
        vm.questionid = parseInt($stateParams.id);

        //#endchoice

        //#choice functions

        vm.addEdit = addEdit;
        vm.mobileClick = mobileClick;
        vm.bulkDeleteChoices = bulkDeleteChoices;

        //#endchoice

        activate();

        //#choice implementaions

        function activate() {
            var promise = [choiceService.query({ questionid: vm.questionid })];
            $scope.$emit('progress.show');
            $q.all(promise).then(function (responses) {
                vm.choices = responses[0].data.choices;
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        function bulkDeleteChoices($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Choices?')
               .textContent([vm.selectedChoices.length,
                   ' Selected Choices will be removed.'].join(''))
               .ariaLabel('Delete Choices')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                choiceService.bulkRemove(vm.selectedChoices).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedChoices, function (item) {
                            vm.choices.splice($lodash.findIndex(vm.choices, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' Choice' : ' Choices',
                            ' deleted'].join(''));
                        vm.selectedChoices = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function addEdit(item, $event) {
            var addChoicePopupOptions = {
                locals: {
                    choice: angular.copy(item) //do not use original model
                },
                controller: 'SettingsChoiceModalController as vm',
                templateUrl: 'app/settings/questions/choices/choice-modal.html',
                targetEvent: $event,
                clickOutsideToClose: true
            };
            $mdDialog.show(addChoicePopupOptions).then(function (choice) {

                if (!item.id) {
                    //add mode
                    vm.choices.push(choice);
                } else {
                    //edit mode
                    var updateChoice = $lodash.find(vm.choices, { id: parseInt(choice.id) });
                    updateChoice.name = choice.name;
                    updateChoice.answer = choice.answer;
                    updateChoice.status = choice.status;
                }

                logger.success(['Choice ', choice.name, (!item.id ? ' Added' : ' Updated')].join(''));
            });
        }

        function mobileClick(item) {
            $state.go('settings.choices.choices', { id: item.id });
        }

        //#endchoice
    }
})();
