(function () {
    'use strict';

    angular
        .module('app.settings.questions')
        .controller('SettingsQuestionModalController', SettingsQuestionModalController);

    /* @ngInject */
    function SettingsQuestionModalController(
        $mdDialog,
        $q,
        $scope,
        $stateParams,
        questionService,
        question,
        logger) {

        var vm = this;
        vm.question = question;
        vm.oldQuestion = angular.copy(question);
        vm.searchText = '';
        vm.hide = hide;
        vm.save = save;
        vm.loadQuestions = loadQuestions;
        vm.questions = [];

        activate();

        function activate() {

            var promises = [loadQuestions()];
            $scope.$emit('progress.show');
            $q.all(promises).then(function (responses) {
            }, function () {
                logger.warning('Opps, something went wrong!');
            }).finally(function () {
                $scope.$emit('progress.hide');
            });

        }

        function hide() {
            $mdDialog.cancel();
        }

        function save() {
            //validation must occur here
            $scope.$emit('progress.show');
            questionService.save(vm.question).then(function (response) {
                //expected result to be the newly added question with the id from api
                if (response.status === 200) {
                    $mdDialog.hide(response.data.question);
                } else {
                    $scope.questionForm.question.$setValidity('duplicate', false);
                }
            }).finally(function () {
                //allways hide progress success or fail
                $scope.$emit('progress.hide');
            });
        }

        function loadQuestions() {
            return vm.questions.length || questionService.query({ subquestion: false }).then(function (response) {
                vm.questions = response.data.questions;
            });
        }

    }
})();
