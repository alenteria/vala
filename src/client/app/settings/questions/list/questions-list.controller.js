(function () {
    'use strict';

    angular
        .module('app.settings.questions')
        .controller('SettingsQuestionsListController', SettingsQuestionsListController);

    /* @ngInject */
    function SettingsQuestionsListController(
        $lodash,
        $mdDialog,
        $mdMedia,
        $q,
        $scope,
        $state,
        $stateParams,
        questionService,
        logger
        ) {
        var vm = this;

        //#question models

        vm.selectedQuestions = [];
        vm.questions = [];
        vm.query = { order: 'name', page: 1, limit: 10 };

        //#endquestion

        //#question functions

        vm.addEdit = addEdit;
        vm.mobileClick = mobileClick;
        vm.bulkDeleteQuestions = bulkDeleteQuestions;

        //#endquestion

        activate();

        //#question implementaions

        function activate() {
            var promise = [questionService.query()];
            $scope.$emit('progress.show');
            $q.all(promise).then(function (responses) {
                vm.questions = responses[0].data.questions;
            }).finally(function () {
                $scope.$emit('progress.hide');
            });
        }

        function bulkDeleteQuestions($event) {
            var confirm = $mdDialog.confirm()
               .title('Would you like to delete selected Questions?')
               .textContent([vm.selectedQuestions.length,
                   ' Selected Questions will be removed.'].join(''))
               .ariaLabel('Delete Questions')
               .targetEvent($event)
               .ok('Delete')
               .cancel('Cancel');
            $mdDialog.show(confirm).then(function () {
                $scope.$emit('progress.show');
                questionService.bulkRemove(vm.selectedQuestions).then(function (response) {
                    if (response.status === 200) {
                        $lodash.forEach(vm.selectedQuestions, function (item) {
                            vm.questions.splice($lodash.findIndex(vm.questions, 'id', item.id), 1);
                        });
                        logger.info([response.data.count,
                            response.data.count === 1 ? ' Question' : ' Questions',
                            ' deleted'].join(''));
                        vm.selectedQuestions = [];
                    } else {
                        logger.warning(response.data.message);
                    }
                }, function () {
                    logger.warning('Unable to process request!');

                }).finally(function () {
                    $scope.$emit('progress.hide');
                });
            });
        }

        function addEdit(item, $event) {
            var addQuestionPopupOptions = {
                locals: {
                    question: angular.copy(item) //do not use original model
                },
                controller: 'SettingsQuestionModalController as vm',
                templateUrl: 'app/settings/questions/list/question-modal.html',
                targetEvent: $event,
                clickOutsideToClose: true
            };
            $mdDialog.show(addQuestionPopupOptions).then(function (question) {

                if (!item.id) {
                    //add mode
                    vm.questions.push(question);
                } else {
                    //edit mode
                    var updateQuestion = $lodash.find(vm.questions, { id: parseInt(question.id) });
                    updateQuestion.name = question.name;
                    updateQuestion.question = question.question;
                    updateQuestion.status = question.status;
                }

                logger.success(['Question ', question.name, (!item.id ? ' Added' : ' Updated')].join(''));
            });
        }

        function mobileClick(item) {
            $state.go('settings.questions.choices', { id: item.id });
        }

        //#endquestion
    }
})();
