(function () {
    'use strict';
    angular.module('app.filters', [])
    .filter('humanize', function() {
        return function(input) {
            input = input || '';
            input = input.replace(/_/g, ' ');
            return input.replace(/\w\S\*/g, function(txt) {
                return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
            });
        };
    });
})();

