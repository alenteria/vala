(function () {
    'use strict';

    angular
        .module('app.register')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['logger', '$auth', '$state'];
    /* @ngInject */
    function RegisterController(logger, $auth, $state) {
        var vm = this;
        vm.title = 'Register';
        vm.user = {
            firstname: '',
            lastname: '',
        };
        vm.signup = signup;

        activate();

        function activate() {
            logger.info('Activated Register View');
        }

        function signup() {
            $auth.signup(vm.user)
              .then(function () {
                  $state.go('login');
                  logger.info('You have successfully created a new account');
              })
              .catch(function (response) {
                  logger.error(response.data.message);
              });
        }
    }
})();
