(function() {
    'use strict';

    angular
        .module('app.regionsettings')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'regionsettings',
                config: {
                    url: '/regionsettings/:id',
                    templateUrl: 'app/regionsettings/regionsettings.html',
                    title: 'Privacy Policy',
                    controller: 'RegionSettingsController',
                    controllerAs: 'vm',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-globe"></i> Region Settings'
                    },
                    resolve: {
                        skipIfLoggedIn: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
