(function () {
    'use strict';

    angular
        .module('app.regionsettings')
        .controller('RegionSettingsController', RegionSettingsController);

    RegionSettingsController.$inject = ['logger', '$auth', '$state'];
    /* @ngInject */
    function RegionSettingsController(logger, $auth, $state) {
        var vm = this;
        vm.title = 'Register';
        vm.regions = [];

        activate();

        function activate() {
            getRegions();
        }

        function getRegions() {
            vm.regions.push({
                id: 1,
                name: 'South West',
                icon: 'fa-star'
            });
            vm.regions.push({
                id: 1,
                name: 'North West',
                icon: 'fa-arrow-right'
            });
            vm.regions.push({
                id: 1,
                name: 'North East',
                icon: 'fa-heart-o'
            });
        }
    }
})();
