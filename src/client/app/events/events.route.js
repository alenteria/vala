(function () {
    'use strict';

    angular
        .module('app.events')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'events',
                config: {
                    url: '/events',
                    templateUrl: 'app/events/events.html',
                    controller: 'EventsController',
                    controllerAs: 'vm',
                    title: 'Events',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-user"></i> Events'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
