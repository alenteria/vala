(function () {
    'use strict';

    angular
        .module('app.layout')
        .controller('ShellController', ShellController);

    /* @ngInject */
    function ShellController(
                    $auth,
                    $interval,
                    $location,
                    $mdDialog,
                    $mdMedia,
                    $mdSidenav,
                    $mdTheming,
                    $q,
                    $rootScope,
                    $scope,
                    $state,
                    $stateParams,
                    $timeout,
                    $window,
                    config,
                    logger,
                    menuService,
                    routerHelper,
                    SatellizerStorage,
                    userService
                    ) {

        $rootScope.showSplash = true;

        var vm = this,
            lastDigestRun = new Date(),
            idlelimit = 1000 * 60 * 15, //15 Min
            idleCheck,
            listToResolve = [];
        var states = routerHelper.getStates();
        vm.busyMessage = 'Please wait ...';
        vm.isBusy = true;
        vm.loginDialog = false;
        vm.layout = getLayout;
        vm.state = $state;
        vm.logout = logout;
        vm.navline = {
            title: config.appTitle,
            text: 'Vala 2.0',
            link: '#'
        };
        vm.stateParams = $stateParams;
        vm.back = back;

        //vm.user = userService.session.user;

        vm.broadcast = function broadcast(channel, data) {
            $rootScope.$broadcast(channel, data);
        };

        $rootScope.$watch(function detectIdle() {
            lastDigestRun = new Date();
        });

        $interval(function () {
            var now = new Date();
            //inactivity timer is 15 min
            if (now - lastDigestRun > idlelimit && !vm.loginDialog) {
                //invalidate session
                $auth.logout();
                //ask for password
                showLoginModal();
            }
        }, idlelimit + 1000); //1sec allowance

        $rootScope.$on('progress.show', function () {
            vm.loading = true;
        });

        $rootScope.$on('progress.hide', function () {
            vm.loading = false;
        });

        $rootScope.$on('splash.show', function () {
            vm.showSplash = true;
        });

        $rootScope.$on('splash.hide', function () {
            vm.showSplash = false;
        });

        vm.navigate = function navigate(item) {
            if (!$mdMedia('gt-sm')) {
                $mdSidenav('left').close();
            }
            if (typeof item === 'string') {
                return $state.go(item);
            } else if (typeof item.state === 'string') {
                $state.go(item.state);
            } else if (typeof item.name === 'string') {
                $state.go(item.name);
            }
        };

        vm.menu = [{
            state: 'dashboard.widgets',
            link: '/',
            title: 'Dashboard',
            icon: 'mdi-view-dashboard'
        },
        {
            state: 'profile',
            link: '/profile',
            title: 'Profile',
            icon: 'mdi-account'
        },
        {
            state: 'events',
            link: '/events',
            title: 'Events',
            icon: 'mdi-calendar-text'
        }];

        vm.admin = [];

        vm.toggleSidenav = function (menuId) {
            $mdSidenav(menuId).toggle();
        };

        vm.breadcrumbs = [];

        vm.applyTheme = function applyTheme(value) {
            vm.theme = value;
        };

        activate();

        function activate() {
            var promises = [menuService.getAdminMenuList()];
            $q.all(promises).then(function (responses) {
                vm.admin = responses[0].data;
            });
            //logger.success(config.appTitle + ' loaded!');
            $rootScope.$on('$stateChangeSuccess', function (state) {
                setBreadcrumbs();
            });
            $scope.$on('session.expire', function (evt, message) {
                listToResolve.push(message.deferred);
                if (!vm.loginDialog) {
                    logger.info('Login Expired!, Please login again...', message);
                    showLoginModal().then(function () {
                        angular.forEach(listToResolve, function (val, key) {
                            val.resolve();
                        });
                        listToResolve = [];
                    });
                }
            });
            if (!$auth.isAuthenticated() && !vm.loginDialog) {
                $timeout(function () {
                    if ($state.current.name === 'login') {
                        return;
                    }
                    if (!SatellizerStorage.get('vala.user.email')) {
                        $window.location.href = '/login';
                    }
                    showLoginModal().then(function () {
                        logger.info('Login Success!');
                        $window.location.reload();
                        //$state.transitionTo($state.current, $stateParams, {
                        //    reload: true,
                        //    inherit: true,
                        //    notify: true
                        //});
                    });
                });
            }
            hideSplash();
        }

        function showLoginModal() {
            var options = {
                templateUrl: 'app/layout/partials/shell-login-modal.partial.html',
                controller: 'LoginController',
                controllerAs: 'vm'
            }, dlg = $mdDialog.show(options);
            vm.loginDialog = true;
            dlg.finally(function () {
                vm.loginDialog = false;
            });
            return dlg;
        }

        function hideSplash() {
            //Force a 1 second delay so we can see the splash.
            $timeout(function () {
                vm.showSplash = false;
            }, 1000);
        }

        function setBreadcrumbs() {
            if (!states) {
                return;
            }
            var statename = [], cur = vm.state.current;
            vm.breadcrumbs.splice(0, vm.breadcrumbs.length);
            while (cur.settings.parent) {
                var route = routerHelper.getStateByName(states, cur.settings.parent);
                vm.breadcrumbs.unshift(route);
                cur = route;
            }
            vm.breadcrumbs.push(vm.state.current);
        }

        function logout() {
            $auth.logout()
              .then(function () {
                  logger.info('You have been logged out');
                  $state.go('login');
              });
        }

        function back() {
            $window.history.back();
        }

        function getLayout() {
            if ($state.current && $state.current.settings && $state.current.settings.layout) {
                return $state.current.settings.layout;
            } else {
                return 'app/layout/partials/shell-authenticated.partial.html';
            }
        }

    }
})();
