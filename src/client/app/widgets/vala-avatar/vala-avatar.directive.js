(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('valaAvatar', valaAvatar);

    /* @ngInject */
    function valaAvatar() {
        return {
            scope: {
                imageUrl: '@'
            },
            //controllerAs: 'vm',
            //bindToController: true,
            templateUrl: 'app/widgets/vala-avatar/vala-avatar.html'
        };
    }
})();
