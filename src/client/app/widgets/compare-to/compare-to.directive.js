(function () {
    'use strict';

    angular
        .module('app.widgets')
        .directive('compareTo', compareTo);

    compareTo.$inject = [];
    /* @ngInject */
    function compareTo() {
        return {
            require: 'ngModel',
            scope: {
                otherModelValue: '=compareTo'
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue === scope.otherModelValue;
                };

                scope.$watch('otherModelValue', function () {
                    ngModel.$validate();
                });
            }
        };
    }
})();
