(function () {
    'use strict';

    angular.module('app', [
        'app.core',
        'app.services',
        'app.filters',
        'app.widgets',
        'app.dashboard',
        'app.settings',
        'app.layout',
        'app.events',
        'app.login',
        'app.profile',
        'app.privacy',
        'app.regionsettings',
        'app.register'
    ]);

})();
