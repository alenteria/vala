(function () {
    'use strict';

    angular
        .module('blocks.logger')
        .factory('logger', logger);

    /* @ngInject */
    function logger($log, $mdToast, $mdTheming) {
        var service = {
            showToasts: true,

            error: error,
            info: info,
            success: success,
            warning: warning,

            // straight to console; bypass toastr
            log: $log.log
        };

        return service;
        /////////////////////

        function error(message, data, title) {
            showToast('<md-icon class="md-warn mdi mdi-24px mdi-minus-circle"></md-icon> ', message);
            $log.error('Error: ' + (message || 'Oops! Something went wrong...'), data);
        }

        function info(message, data, title) {
            showToast('<md-icon class="md-primary mdi mdi-24px mdi-bell"></md-icon> ', message);
            $log.info('Info: ' + message, data);
        }

        function success(message, data, title) {
            showToast('<md-icon class="md-accent mdi mdi-24px mdi-checkbox-marked-circle"></md-icon> ', message);
            $log.info('Success: ' + message, data);
        }

        function warning(message, data, title) {
            showToast('<md-icon class="md-warn md-hue-1 mdi mdi-24px mdi-alert-circle"></md-icon> ', message);
            $log.warn('Warning: ' + (message || 'Oops! Something went wrong...'), data);
        }

        function showToast(title, text) {
            $mdToast.show({
                theme: $mdTheming.defaultTheme(),
                template: ['<md-toast>',
                            '<span layout-padding>', text, '</span>',
                            '</md-toast> '].join(''),
                hideDelay: 3000,
                position: 'bottom right'
            });
        }
    }
}());
