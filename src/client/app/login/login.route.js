(function () {
    'use strict';

    angular
        .module('app.login')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'login',
                config: {
                    url: '/login',
                    templateUrl: 'app/login/login.html',
                    controller: 'LoginController',
                    controllerAs: 'vm',
                    title: 'Login',
                    settings: {
                        layout: 'app/layout/partials/shell-public.partial.html',
                        nav: 0,
                        content: '<i class="fa fa-lock"></i> Login'
                    },
                    resolve: {
                        skipIfLoggedIn: routerHelper.skipIfLoggedIn
                    }
                }
            }
        ];
    }
})();
