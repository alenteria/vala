(function () {
    'use strict';

    angular
        .module('app.login')
        .controller('LoginController', LoginController);

    /* @ngInject */
    function LoginController($scope, logger, $state, $auth, $location, $mdDialog, SatellizerStorage) {
        var vm = this;
        vm.title = 'Login';
        vm.user = { email: SatellizerStorage.get('vala.user.email'), password: '' };
        vm.login = login;
        vm.logout = logout;
        vm.authenticate = authenticate;
        vm.retry = 0;

        activate();

        function authenticate(provider) {
            $auth.authenticate(provider)
            .then(function (response) {
                logger.success('You have successfully signed in');
                $state.go('dashboard.widgets');
            })
            .catch(function (response) {
                logger.error(response.data.message);
            });
        }

        function activate() {
            $scope.$emit('progress.hide');
            $scope.$emit('splash.hide');
        }

        function login(noRetry) {
            if ($state.current.name === 'login') {
                $scope.$emit('splash.show');
            } else {
                $scope.$emit('progress.show');
            }
            logger.success('Logging in...');
            $auth.login(vm.user)
              .then(function () {
                  vm.retry = 0;
                  SatellizerStorage.set('vala.user.email', vm.user.email);
                  $scope.$emit('splash.hide');
                  $scope.$emit('progress.hide');
                  logger.success('You have successfully signed in');
                  $mdDialog.hide();
                  if ($state.current.name === 'login') {
                      $state.transitionTo('dashboard.widgets');
                  }
              })
              .catch(function (response) {
                  $scope.$emit('splash.hide');
                  $scope.$emit('progress.hide');
                  logger.warning(response.data ? response.data.message : 'Opps! Something went wrong.', response.status);
                  if (!response.data && noRetry !== true && vm.retry === 0) {
                      logger.info('Retrying....');
                      vm.retry = vm.retry + 1;
                      login(true);
                  }
              });
        }

        function logout() {
            $auth.logout()
              .then(function () {
                  $mdDialog.hide();
                  logger.info('You have been logged out');
                  $state.go('login');
              });
        }
    }
})();
