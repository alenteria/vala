(function() {
    'use strict';

    angular
        .module('app.privacy')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'privacy',
                config: {
                    url: '/privacy',
                    templateUrl: 'app/privacy/privacy.html',
                    title: 'Privacy Policy',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-eye"></i> Privacy Policy'
                    },
                    resolve: {
                        skipIfLoggedIn: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
