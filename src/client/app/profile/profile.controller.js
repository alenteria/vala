(function () {
    'use strict';

    angular
        .module('app.profile')
        .controller('ProfileController', ProfileController);

    /* @ngInject */
    function ProfileController($q, userService, logger, $state) {
        var vm = this;
        vm.title = 'Profile';
        vm.user = {};

        activate();

        function activate() {
            var promises = [userService.getProfile()];
            return $q.all(promises).then(function (resposnes) {
                vm.user = resposnes[0].data;
                logger.info('Activated Profile View');
            });
        }
    }
})();
