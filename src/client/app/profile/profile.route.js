(function () {
    'use strict';

    angular
        .module('app.profile')
        .run(appRun);

    appRun.$inject = ['routerHelper'];
    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(routerHelper));
    }

    function getStates(routerHelper) {
        return [
            {
                state: 'profile',
                config: {
                    url: '/profile',
                    templateUrl: 'app/profile/profile.html',
                    controller: 'ProfileController',
                    controllerAs: 'vm',
                    title: 'Profile',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-user"></i> Profile'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            },
            {
                state: 'profile/edit',
                config: {
                    url: '/profile/edit',
                    templateUrl: 'app/profile/profile-edit.html',
                    controller: 'ProfileEditController',
                    controllerAs: 'vm',
                    title: 'Edit Profile',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-pencil"></i> Edit Profile'
                    },
                    resolve: {
                        loginRequired: routerHelper.loginRequired
                    }
                }
            }
        ];
    }
})();
