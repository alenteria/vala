(function () {
    'use strict';

    angular
        .module('app.settings.users')
        .controller('ProfileEditController', ProfileEditController);

    /* @ngInject */
    function ProfileEditController($q, userService, organizationsService,
        logger, $state, $scope, Upload, $stateParams) {
        var vm = this;
        vm.active = 1;
        vm.type = 1;
        vm.roleName = 'Super Admin';
        vm.role = 1;
        vm.genders = [{ name: 'male' }, { name: 'female' }];
        vm.counties = [{ id: 1, name: 'Dorset' }, { id: 2, name: 'Devon' }];
        vm.user = {};
        vm.industrie = [];

        vm.uploadPicture = uploadPicture;
        vm.uploadOrgPicture = uploadOrgPicture;

        activate();

        function activate() {
            return userService.getProfile().then(function (response) {
                vm.user = response.data.user;
                vm.user.position = 'Founder of Venus Award';
                vm.user.about = 'There are many variations of passages of ' +
                'Lorem Ipsum available, but the majority' +
                'have suffered alteration in some form Ipsum available.';
                return vm.user;
            });
        }

        // upload on file select
        function uploadPicture(file) {
            userService.uploadPicture({ file: file, 'type': 'user' }).then(function (resp) {
                console.log('uploaded. Response: ' + resp.data);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        }
        // upload on file select
        function uploadOrgPicture(file) {
            organizationsService.uploadPicture({ file: file, 'type': 'org' }).then(function (resp) {
                console.log('uploaded. Response: ' + resp.data);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            });
        }

    }
})();

// (function () {
//     'use strict';

//     angular
//         .module('app.profile')
//         .controller('ProfileEditController', ProfileEditController);

//     /* @ngInject */
//     function ProfileEditController($q, userService, logger, $state, Upload, SITE_CONFIG) {
//         var vm = this;

//         activate();

//         function activate() {
//             var promises = [getProfile()];
//             return $q.all(promises).then(function () {
//                 logger.info('Activated Edit Profile View');
//             });
//         }

//         function getProfile() {
//             return userService.getProfile().then(function (response) {
//                 vm.user = response.data.user;
//                 vm.user.position = 'Founder of Venus Award';
//                 vm.user.about = 'There are many variations of passages of ' +
//                 'Lorem Ipsum available, but the majority' +
//                 'have suffered alteration in some form Ipsum available.';
//                 return vm.user;
//             });
//         }

//         vm.updateProfile = function () {
//             userService.updateProfile(vm.user)
//                 .then(
//                     function (response) {
//                         if (response.data.status !== 200) {
//                             logger.error(response.data.message);
//                             return;
//                         }
//                         logger.success(response.data.message);
//                     },
//                     function (response) {
//                         logger.error(response.data.message);
//                     }
//                     );
//         };

//         vm.upload = function (file) {
//             Upload.upload({
//                 url: SITE_CONFIG.API_URL + 'user/profilePicUpload',
//                 data: { file: file }
//             }).then(function (resp) {
//                 console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
//             }, function (resp) {
//                 console.log('Error status: ' + resp.status);
//             }, function (evt) {
//                 var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
//                 console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
//             });
//         };
//     }
// })();
