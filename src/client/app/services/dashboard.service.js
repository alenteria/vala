(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('dashboardService', dashboardService);

    dashboardService.$inject = ['$http', '$q', 'exception', 'SatellizerConfig'];

    /* @ngInject */
    function dashboardService($http, $q, exception, SatellizerConfig) {
        var baseUrl = SatellizerConfig.baseUrl,

            service = {
                getEnabledUserRoleModules: getEnabledUserRoleModules,
                getUserRoleModules: getUserRoleModules
            };

        return service;

        function getEnabledUserRoleModules() {
            return $http.get(baseUrl + 'dashboard/enabled-dashboard-modules-by-user-role');
        }

        function getUserRoleModules() {
            return $http.get(baseUrl + 'dashboard/dashboard-modules-by-user-role');
        }
    }
})();
