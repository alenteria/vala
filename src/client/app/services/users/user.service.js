(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('userService', userService);

    userService.$inject = ['$http', '$q', 'exception', 'SatellizerConfig'];
    /* @ngInject */
    function userService($http, $q, exception, SatellizerConfig) {
        var baseUrl = SatellizerConfig.baseUrl,
            serviceCache = {
                user: null,
                role: null,
                count: null,
            },
            service = {
                query: query,
                getProfile: getProfile,
                updateProfile: updateProfile,
                updatePassword: updatePassword,
                updatePicture: updatePicture,
                uploadPicture: uploadPicture,
                bulkRemove: bulkRemove,
                getUsers: getUsers,
                getCount: getCount,
                getUserById: getUserById
            };

        return service;

        function getUsers(query) {
            return $http.get(baseUrl + 'user/list?page=' + query.page + '&limit=' + query.limit);
        }

        function getCount(getFromCache) {
            //if( getFromCache === true && serviceCache.userCount )
            //  return $q.when(serviceCache.userCount);
            return $http.get(baseUrl + 'user/count');
        }

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            //return $http.get(baseUrl + 'users?_=' + new Date().getTime());
            var mockusers = [];
            for (var i = 0; i < 100; i++) {
                mockusers.push({
                    id: i + 1,
                    picture: '/images/faces/' + (i % 9) + '.jpg',
                    displayName: 'David Frankel ' + i,
                    firstname: 'David ' + i,
                    lastname: 'Frankel',
                    MI: i % 2 === 0 ? 'M' : 'F',
                    email: i + '_davidfvalasystem@gmail.com',
                    gender: i % 2 === 0 ? 1 : 2,
                    active: true,
                    verified: true,
                    birth_date: new Date(),
                    date_registered: new Date(),
                    facebook: { id: '11213131', username: 'user.name', link: 'fb.com/user.name' },
                    linkedin: { id: '5164681313', username: 'user.name', link: 'linkedin.com/5164681313' },
                    twitter: { id: '87191145', username: '@username', link: 'twitter.com/username' },
                    googleplus: {},
                    regions: [{
                        id: 1,
                        name: 'Dorset'
                    }],
                    regional_area: 'Southampon',
                    address: 'Dorset',
                    address2: 'Dorset',
                    city: 'Bournemouth',
                    county: 1,
                    postcode: '12344',
                    phone_number: '12345778',
                    work_number: '12345778',
                    mobile_number: '12345778',
                    job_title: 'CEO',
                    org_name: 'Venus Award',
                    org_industry: 1,
                    org_address: 'Dorset',
                    org_address2: 'Dorset',
                    org_city: 'Bournemouth',
                    org_county: 1,
                    org_website: 'venusawards.com',
                    org_logo: '',
                    org_postcode: '12344',
                    org_sponsor_id: 0,
                    biography: 'Living in a world full of work.'
                });
            }
            return $q.when({
                status: 200,
                data: mockusers
            });
        }

        function getUserById(id) {
            return $http.get(baseUrl + 'user/get-by-id/' + id).then(
                function (response) {
                    var user = response.data.user;
                    user.profile.birth_date = new Date(user.profile.birth_date);
                    return user;
                }
            );
        }

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function getProfile(getFromCache) {
            return $http.get(baseUrl + 'user/me?_=' + new Date().getTime());
            //result will be based on user prole, if admin return adin viewable
            //if sopnsor or user that have limitted access, return only viewable
            //mock
            //if (getFromCache === true && serviceCache.user) {
            //    return $q.when(serviceCache.user);
            //} else {
            //    var promise = $q.when({
            //        status: 200,
            //        data: {
            //            id: 1,
            //            picture: '/images/faces/0.jpg',
            //            displayName: 'David Frankel',
            //            firstname: 'David',
            //            lastname: 'Frankel',
            //            MI: 'M',
            //            email: 'davidfvalasystem@gmail.com',
            //            gender: 1,
            //            active: true,
            //            verified: true,
            //            birth_date: new Date(),
            //            registred_date: new Date(),
            //            facebook: { id: '11213131', username: 'user.name', link: 'fb.com/user.name' },
            //            linkedin: { id: '5164681313', username: 'user.name', link: 'linkedin.com/5164681313' },
            //            twitter: { id: '87191145', username: '@username', link: 'twitter.com/username' },
            //            googleplus: {},
            //            regions: [{
            //                id: 1,
            //                name: 'Dorset'
            //            }],
            //            regional_area: 'Southampon',
            //            address: 'Dorset',
            //            address2: 'Dorset',
            //            city: 'Bournemouth',
            //            county: 1,
            //            postcode: '12344',
            //            phone_number: '12345778',
            //            work_number: '12345778',
            //            mobile_number: '12345778',
            //            job_title: 'CEO',
            //            org_name: 'Venus Award',
            //            org_industry: 1,
            //            org_address: 'Dorset',
            //            org_address2: 'Dorset',
            //            org_city: 'Bournemouth',
            //            org_county: 1,
            //            org_website: 'venusawards.com',
            //            org_logo: '',
            //            org_postcode: '12344',
            //            org_sponsor_id: 0,
            //            biography: 'Living in a world full of work.',
            //            role: {
            //                id: 1,
            //                name: 'superadmin'
            //            }
            //        }
            //    });
            //    promise.then(function (response) {
            //        serviceCache.user = response;
            //    });
            //    return promise;
            //}
        }

        function bulkRemove(users) {
            //mockup go to return for backend implementation //fake delete
            var count = 0;

            return $q.when({
                status: 200,
                data: {
                    count: users.length
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/users/bulkdelete/?_=' + new Date().getTime(), roles);
        }

        //HTTP VERB(put) : CTRL(user) / ACTION(edit)
        //verify payload on the server for user updating the profile data for security role and user.id
        //expect all updatable field on table user but you have to validate by roles on the server and throw some error
        //I'll have validationon client side but still safe to validate also on the server
        function updateProfile(profileData) {
            //return $http.put(baseUrl + 'user/edit/'+profileData.id, profileData);
            //mock
            return $q.when({
                status: 200
            });
        }

        //HTTP VERB(put) : CTRL(user) / ACTION(password)
        //verify payload on the server for user updating the profile data for security role and user.id
        //expect all updatable field on table user but you have to validate by roles on the server and throw some error
        //I'll have validationon client side but still safe to validate also on the server
        function updatePassword(passwordData) {
            //return $http.put(baseUrl + 'user/password/'+profileData.id, profileData);
            //mock
            return $q.when({
                status: 200
            });
        }

        //HTTP VERB(put) : CTRL(user) / ACTION(picture)
        //verify payload on the server for user updating the profile data for security role and user.id
        //expect all updatable field on table user but you have to validate by roles on the server and throw some error
        //I'll have validationon client side but still safe to validate also on the server
        function updatePicture(pictureData) {
            //return $http.post(baseUrl + 'user/picture/'+profileData.id, profileData);
            //mock
            return $q.when({
                status: 200
            });
        }

        //HTTP VERB(post) : CTRL(user) / ACTION(picture)
        //verify payload on the server for user updating the profile data for security role and user.id
        //expect all updatable field on table user but you have to validate by roles on the server and throw some error
        //I'll have validationon client side but still safe to validate also on the server
        function uploadPicture(pictureData) {
            //return $http.post(baseUrl + 'user/picture/'+profileData.id, profileData);
            //mock/expected return
            return $q.when({
                status: 200,
                data: {
                    base64: '', //base64 encoded image temporary while not saved the picture
                    url: '' //actual url, might be cdn or google cloud storage when available
                }
            });
        }
    }
})();
