(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('cityService', cityService);

    /* @ngInject */
    function cityService($http, $lodash, $q, exception, SatellizerConfig) {
        var serviceCache = {
            cities: [
                { regionid: 1, countyid: 1, id: 1,postcodes: ['SO','BD','GD'], name: 'Birmingham', status: 1 },
                { regionid: 3, countyid: 1, id: 2,postcodes: ['ES','EM'], name: 'Wolverhampton', status: 1 },
                { regionid: 5, countyid: 1, id: 3,postcodes: ['GA','ES2'], name: 'Dorset', status: 1 },
                { regionid: 5, countyid: 1, id: 4,postcodes: ['CA','GE2'], name: 'Devon', status: 1 },
                { regionid: 5, countyid: 2, id: 5,postcodes: ['ISS','WDR'], name: 'Devon2', status: 1 }
            ]
        },
        service = {
            query: query,
            getById: getById,
            bulkRemove: bulkRemove,
            save: save
        };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            var param = {};
            if (parseInt(filter.regionid)) {
                param.regionid = parseInt(filter.regionid);
            }
            if (parseInt(filter.countyid)) {
                param.countyid = parseInt(filter.countyid);
            }
            return $q.when({
                status: 200,
                data: {
                    cities: angular.copy($lodash.filter(serviceCache.cities, param)) || [] //no direct updating on cache object
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/cities/'+filter.regionid+'?_=' + new Date().getTime());
        }

        function getById(id) {
            return $q.when({
                status: 200,
                data: {
                    city: angular.copy($lodash.findWhere(serviceCache.cities, { 'id': parseInt(id) }))
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/cities/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(regionid, cities) {
            var count = 0;
            $lodash.forEach(cities, function (item) {
                serviceCache.cities.splice($lodash.findIndex(serviceCache.cities, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/cities/bulkdelete/'
            //  + regionid + '?_=' + new Date().getTime(), cities);
        }

        function save(city) {
            if (!city.id) {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.cities, { 'name': city.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Coounty name conflict! Duplicate County name not Allowed.'
                        }
                    });
                }
                var newCounty = angular.copy(city);
                newCounty.regionid = city.regionid;
                newCounty.countyid = city.countyid;
                newCounty.status = 1;
                newCounty.id = new Date().getTime();
                serviceCache.cities.push(newCounty);
                return $q.when({
                    status: 200,
                    data: {
                        city: newCounty
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/cities/add/', city)
                //        .then(function (response) {
                //            return response.data;
                //        });
            } else {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.cities, { 'name': city.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Coounty name conflict! Duplicate County name not Allowed.'
                        }
                    });
                }
                var updateCounty = $lodash.findWhere(serviceCache.cities, { 'id': parseInt(city.id) });
                updateCounty.status = city.status;
                updateCounty.name = city.name;
                return $q.when({
                    status: 200,
                    data: {
                        city: updateCounty
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/cities/update/' + city.id, city)
                //        .then(function (response) {
                //            return response.data;
                //        });
            }
        }
    }
})();
