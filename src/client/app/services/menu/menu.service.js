(function () {
    'use strict';

    angular
        .module('app.services')
        .service('menuService', menuService);

    /* @ngInject */
    function menuService($q, $lodash, routerHelper) {

        var service = {
            getSidebarMenuList: getSidebarMenuList,
            getAdminMenuList: getAdminMenuList,
        };

        return service;

        function getSidebarMenuList(roleId) {
            var states = routerHelper.getStates();
            //filter menu by role roleId
            return states;
        }

        function getAdminMenuList(roleId) {
            var states = routerHelper.getStates(), result = [];
            //filter menu by role roleId
            $lodash.forEach(states, function (item, key) {
                if (!item.abstract && item.settings && item.settings.admin) {
                    result.push($lodash.clone(item));
                }
            });
            return $q.when({
                status: 200,
                data: result
            });
        }

    }
})();
