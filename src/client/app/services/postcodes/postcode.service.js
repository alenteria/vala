(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('postcodeService', postcodeService);

    /* @ngInject */
    function postcodeService($http, $lodash, $q, exception, SatellizerConfig) {
        var serviceCache = {
            postcodes: [
                { regionid: 1, countyid: 1, city: 1, id: 1, name: 'SO', status: 1 },
                { regionid: 1, countyid: 2, city: 1, id: 2, name: 'SE', status: 1 },
                { regionid: 4, countyid: 1, city: 1, id: 3, name: 'CA', status: 1 },
                { regionid: 5, countyid: 1, city: 1, id: 4, name: 'GA', status: 1 }
            ]
        },
        service = {
            query: query,
            getById: getById,
            bulkRemove: bulkRemove,
            save: save
        };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            return $q.when({
                status: 200,
                data: {
                    postcodes: angular.copy($lodash.filter(serviceCache.postcodes, {
                        regionid: parseInt(filter.regionid)
                    })) || [] //no direct updating on cache object
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/postcodes/'+filter.cityid+'?_=' + new Date().getTime());
        }

        function getById(id) {
            return $q.when({
                status: 200,
                data: {
                    postcode: angular.copy($lodash.findWhere(serviceCache.postcodes, { 'id': parseInt(id) }))
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/postcodes/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(regionid, postcodes) {
            var count = 0;
            $lodash.forEach(postcodes, function (item) {
                serviceCache.postcodes.splice($lodash.findIndex(serviceCache.postcodes, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/postcodes/bulkdelete/'
            //  + regionid + '?_=' + new Date().getTime(), postcodes);
        }

        function save(postcode) {
            if (!postcode.id) {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.postcodes, { 'name': postcode.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Coounty name conflict! Duplicate County name not Allowed.'
                        }
                    });
                }
                var newCounty = angular.copy(postcode);
                newCounty.regionid = postcode.regionid;
                newCounty.countyid = postcode.countyid;
                newCounty.cityid = postcode.cityid;
                newCounty.status = 1;
                newCounty.id = new Date().getTime();
                serviceCache.postcodes.push(newCounty);
                return $q.when({
                    status: 200,
                    data: {
                        postcode: newCounty
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/postcodes/add/', postcode)
                //        .then(function (response) {
                //            return response.data;
                //        });
            } else {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.postcodes, { 'name': postcode.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Coounty name conflict! Duplicate County name not Allowed.'
                        }
                    });
                }
                var updateCounty = $lodash.findWhere(serviceCache.postcodes, { 'id': parseInt(postcode.id) });
                updateCounty.status = postcode.status;
                updateCounty.name = postcode.name;
                return $q.when({
                    status: 200,
                    data: {
                        postcode: updateCounty
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/postcodes/update/' + postcode.id, postcode)
                //        .then(function (response) {
                //            return response.data;
                //        });
            }
        }
    }
})();
