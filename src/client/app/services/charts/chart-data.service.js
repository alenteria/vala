(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('chartDataService', chartDataService);

    chartDataService.$inject = ['$http', '$q', 'exception', 'SatellizerConfig'];

    /* @ngInject */
    function chartDataService($http, $q, exception, SatellizerConfig) {
        var baseUrl = SatellizerConfig.baseUrl,
            service = {
                query: query
            };

        return service;

        function query(type, filters) {
            if (type === 'Nominations and Nominees') {
                return getNominationsAndNomineesChartData();
            } else if (type === 'Applications and Applicants') {
                return getApplicationsAndApplicantsChartData();
            } else if (type === 'Ticket Sales') {
                return getTicketSalesChartData();
            } else if (type === 'Sponsors') {
                return getSponsorsChartData();
            } else if (type === 'Events Timeline') {
                return getEventsTimelineChartData();
            } else if (type === 'Regional Map') {
                return getRegionalMapChartData();
            }
        }

        function getNominationsAndNomineesChartData() {
            //filter based on user and role
            //return $http.get(baseUrl + '/', { id: id });
            return $q.when({
                result: 200,
                data: {
                    'cols': [
                        { id: 'r', label: 'Regions', type: 'string' },
                        { id: 'a', label: 'Nominations', type: 'number' },
                        { id: 'b', label: 'Nominees', type: 'number' }
                    ], 'rows': [
                        {
                            c: [
                               { v: 'Dorset' },
                               { v: 13 },
                               { v: 17 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Devon' },
                               { v: 31 },
                               { v: 28 }
                            ]
                        },
                        {
                            c: [
                               { v: 'Southhampon' },
                               { v: 15 },
                               { v: 11 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Postsmouth' },
                               { v: 20 },
                               { v: 42 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Isle of Wight' },
                               { v: 14 },
                               { v: 19 },
                            ]
                        }
                    ]
                }
            });
        }

        function getApplicationsAndApplicantsChartData() {
            //filter based on user and role
            //return $http.get(baseUrl + '/', { id: id });
            return $q.when({
                result: 200,
                data: {
                    'cols': [
                        { id: 'r', label: 'Regions', type: 'string' },
                        { id: 'a', label: 'Applications', type: 'number' },
                        { id: 'b', label: 'Applicants', type: 'number' }
                    ], 'rows': [
                        {
                            c: [
                               { v: 'Dorset' },
                               { v: 90 },
                               { v: 88 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Devon' },
                               { v: 115 },
                               { v: 118 }
                            ]
                        },
                        {
                            c: [
                               { v: 'Southhampon' },
                               { v: 106 },
                               { v: 80 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Postsmouth' },
                               { v: 92 },
                               { v: 60 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Isle of Wight' },
                               { v: 84 },
                               { v: 70 },
                            ]
                        }
                    ]
                }
            });
        }

        function getTicketSalesChartData() {
            //filter based on user and role
            //return $http.get(baseUrl + '/', { id: id });
            return $q.when({
                result: 200,
                data: {
                    'cols': [
                        { id: 'r', label: 'Regions', type: 'string' },
                        { id: 1, label: 'Dorset', type: 'number' },
                        { id: 2, label: 'Devon', type: 'number' },
                        { id: 3, label: 'Southhampon', type: 'number' },
                        { id: 4, label: 'Postsmouth', type: 'number' },
                        { id: 5, label: 'Isle of Wight', type: 'number' }
                    ], 'rows': [
                        {
                            c: [
                               { v: 'September 2015' },
                               { v: 80 },
                               { v: 73 },
                               { v: 35 },
                               { v: 31 },
                               { v: 23 },
                               { v: 58 },
                               { v: 75 },
                            ]
                        },
                        {
                            c: [
                               { v: 'October 2015' },
                               { v: 90 },
                               { v: 73 },
                               { v: 45 },
                               { v: 31 },
                               { v: 20 },
                               { v: 58 },
                               { v: 75 },
                            ]
                        },
                        {
                            c: [
                               { v: 'November 2015' },
                               { v: 10 },
                               { v: 45 },
                               { v: 50 },
                               { v: 52 },
                               { v: 80 },
                               { v: 59 },
                               { v: 60 },
                            ]
                        },
                        {
                            c: [
                               { v: 'December 2015' },
                               { v: 45 },
                               { v: 60 },
                               { v: 90 },
                               { v: 73 },
                               { v: 30 },
                               { v: 86 },
                               { v: 55 },
                            ]
                        }
                    ]
                }
            });
        }

        function getSponsorsChartData() {
            //filter based on user and role
            //return $http.get(baseUrl + '/', { id: id });
            return $q.when({
                result: 200,
                data: {
                    'cols': [
                        { id: 'r', label: 'Regions', type: 'string' },
                        { id: 'a', label: 'Count', type: 'number' }
                    ], 'rows': [
                        {
                            c: [
                               { v: 'Dorset' },
                               { v: 88 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Devon' },
                               { v: 118 }
                            ]
                        },
                        {
                            c: [
                               { v: 'Southhampon' },
                               { v: 80 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Postsmouth' },
                               { v: 60 },
                            ]
                        },
                        {
                            c: [
                               { v: 'Isle of Wight' },
                               { v: 84 },
                            ]
                        }
                    ]
                }
            });
        }

        function getEventsTimelineChartData() {
            //filter based on user and role
            //return $http.get(baseUrl + '/', { id: id });
            function fromToday(days) {
                var date = new Date(), result = new Date(date);
                result.setDate(result.getDate() - 5);
                result.setDate(result.getDate() + days);
                return result;
            }
            return $q.when({
                result: 200,
                data: {
                    'cols': [
                      { type: 'string', id: 'region' },
                      { type: 'string', id: 'name' },
                      { type: 'date', id: 'start' },
                      { type: 'date', id: 'end' },
                    ],
                    'rows': [
                          {
                              'c': [
                                  { 'v': 'Dorset' },
                                  { 'v': 'Pre-Nomination Phase' },
                                  { 'v': fromToday(0) },
                                  { 'v': fromToday(7) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                  { 'v': 'Dorset' },
                                  { 'v': 'Nomination Phase' },
                                  { 'v': fromToday(7) },
                                  { 'v': fromToday(14) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                  { 'v': 'Dorset' },
                                  { 'v': 'Juding Phase' },
                                  { 'v': fromToday(14) },
                                  { 'v': fromToday(15) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                  { 'v': 'Dorset' },
                                  { 'v': 'Awarding Ceremony' },
                                  { 'v': fromToday(15) },
                                  { 'v': fromToday(16) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                { 'v': 'Devon' },
                                { 'v': 'Ticket Sale' },
                                  { 'v': fromToday(0) },
                                  { 'v': fromToday(7) },
                                { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                  { 'v': 'Devon' },
                                  { 'v': 'Pre-Nomination Phase' },
                                  { 'v': fromToday(7) },
                                  { 'v': fromToday(14) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                  { 'v': 'Devon' },
                                  { 'v': 'Nomination Phase' },
                                  { 'v': fromToday(15) },
                                  { 'v': fromToday(15 + 7) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                  { 'v': 'Devon' },
                                  { 'v': 'Juding Phase' },
                                  { 'v': fromToday(15 + 7) },
                                  { 'v': fromToday(15 + 7 + 1) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                  { 'v': 'Devon' },
                                  { 'v': 'Awarding Ceremony' },
                                  { 'v': fromToday(15 + 7 + 1) },
                                  { 'v': fromToday(15 + 7 + 2) },
                                  { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                { 'v': 'Southhampon' },
                                { 'v': 'Ticket Sale' },
                                { 'v': fromToday(17) },
                                { 'v': fromToday(17 + 7) },
                                { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                { 'v': 'Postsmouth' },
                                { 'v': 'Ticket Sale' },
                                { 'v': fromToday(20) },
                                { 'v': fromToday(20 + 4) },
                                { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                { 'v': 'Isle of Wight' },
                                { 'v': 'Ticket Sale' },
                                { 'v': fromToday(22) },
                                { 'v': fromToday(22 + 2) },
                                { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                { 'v': 'Birmingham' },
                                { 'v': 'Ticket Sale' },
                                { 'v': fromToday(23) },
                                { 'v': fromToday(23 + 1) },
                                { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                { 'v': 'West Midlands' },
                                { 'v': 'Ticket Sale' },
                                { 'v': fromToday(18) },
                                { 'v': fromToday(18 + 6) },
                                { 'v': null }
                              ]
                          },
                          {
                              'c': [
                                { 'v': 'DATE' },
                                { 'v': 'TODAY' },
                                { 'v': new Date() },
                                { 'v': new Date() },
                                { 'v': null }
                              ]
                          }
                    ]
                }
            });
        }

        function getRegionalMapChartData() {
            //filter based on user and role
            //return $http.get(baseUrl + '/', { id: id });
            return $q.when({
                result: 200,
                data: {
                    'cols': [
                        { id: 'r', label: 'Regions', type: 'string' },
                        { id: 'a', label: 'Count', type: 'number' }
                    ], 'rows': [
                        {
                            c: [
                               { v: 'West Midlands' },
                               { v: 1 },
                            ]
                        }
                    ]
                }
            });
        }

    }
})();
