(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('chartTypeService', chartTypeService);

    chartTypeService.$inject = ['$http', '$q', 'exception', 'SatellizerConfig'];

    /* @ngInject */
    function chartTypeService($http, $q, exception, SatellizerConfig) {
        //note role leve, any roleLevel greater than or equal to its role will be accessible
        var baseUrl = SatellizerConfig.baseUrl,
            chartTypes = [
                       { id: 8, roleLevel: 4, name: 'Events Timeline', type: 'Timeline' },
                       { id: 1, roleLevel: 4, name: 'Nominations and Nominees', type: 'ColumnChart' },
                       { id: 2, roleLevel: 3, name: 'Applications and Applicants', type: 'ColumnChart' },
                       { id: 3, roleLevel: 1, name: 'Ticket Sales', type: 'LineChart' },
                       { id: 7, roleLevel: 3, name: 'Sponsors', type: 'PieChart' },
                       //{ id: 4, roleLevel: 3, name: 'Categories Per Region' },
                       //{ id: 5, roleLevel: 2, name: 'Sponsors per Categories' },
                       //{ id: 6, roleLevel: 2, name: 'Sponsors Per Regiions' },
            ],
            service = {
                getChartTypeList: getChartTypeList,
                getchartType: getchartType
            };

        return service;

        function getChartTypeList() {
            //return $http.get(baseUrl + 'chartType');
            //mockup need to return from backend later
            return $q.when({
                status: 200,
                data: chartTypes
            });
        }

        function getchartType(id) {
            //return $http.get(baseUrl + 'chartType/', { id: id });
            return $q.when(chartTypes.findOne(function (item) {
                return item.id === id;
            }));
        }
    }
})();
