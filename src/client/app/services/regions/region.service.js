(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('regionService', regionService);

    /* @ngInject */
    function regionService($http, $lodash, $q, exception, SatellizerConfig) {
        var serviceCache = {
            regions: [
            { id: 1, counties: 2, name: 'South West', status: 1 },
            { id: 2, counties: 0, name: 'North West', status: 1 },
            { id: 3, counties: 1, name: 'North East', status: 1 },
            { id: 4, counties: 0, name: 'East of England', status: 1 },
            { id: 5, counties: 2, name: 'East Midlands', status: 1 },
            { id: 6, counties: 0, name: 'London', status: 1 },
            { id: 7, counties: 0, name: 'Yorkshire and the Humber', status: 1 },
            { id: 8, counties: 5, name: 'West Midlands', status: 1 },
            { id: 9, counties: 2, name: 'South East', status: 1 }
            ]
        },
        service = {
            query: query,
            getById: getById,
            bulkRemove: bulkRemove,
            save: save
        };

        return service;

        /*
         *
         * @param cache boolean if 1 use cache else retrieve from server
         */
        function query(filter) {
            return $http.get(SatellizerConfig.baseUrl + 'admin/region-areas?_=' + new Date().getTime());

            //return $q.when({
            //    status: 200,
            //    data: {
            //        regions: angular.copy(serviceCache.regions)
            //    }
            //});
            //return $http.get(SatellizerConfig.baseUrl + 'admin/regions?_=' + new Date().getTime());
        }

        function getById(id) {
            return $http.get(SatellizerConfig.baseUrl + 'admin/region-areas/' + id + '?_=' + new Date().getTime());

            //return $q.when({
            //    status: 200,
            //    data: {
            //        region: angular.copy($lodash.find(serviceCache.regions, { 'id': parseInt(id) }))
            //    }
            //});
            //return $http.get(SatellizerConfig.baseUrl + 'admin/regions/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(regions) {
            var count = 0;
            $lodash.forEach(regions, function (item) {
                serviceCache.regions.splice($lodash.findIndex(serviceCache.regions, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/regions/bulkdelete/'
            //  + regionid + '?_=' + new Date().getTime(), regions);
        }

        function save(region) {
            if (!region.id) {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.regions, { 'name': region.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Region name conflict! Duplicate Region name not Allowed.'
                        }
                    });
                }
                var newRegion = angular.copy(region);
                newRegion.status = 1;
                newRegion.id = new Date().getTime();
                serviceCache.regions.push(newRegion);
                return $q.when({
                    status: 200,
                    data: {
                        region: newRegion
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/regions/add/', region)
                //        .then(function (response) {
                //            return response.data;
                //        });
            } else {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.regions, { 'name': region.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Region name conflict! Duplicate Region name not Allowed.'
                        }
                    });
                }
                var updateRegion = $lodash.findWhere(serviceCache.regions, { 'id': parseInt(region.id) });
                updateRegion.status = region.status;
                updateRegion.name = region.name;
                return $q.when({
                    status: 200,
                    data: {
                        regionArea: updateRegion
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/regions/update/' + region.id, region)
                //        .then(function (response) {
                //            return response.data;
                //        });
            }
        }
    }
})();
