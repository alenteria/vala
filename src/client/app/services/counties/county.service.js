(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('countyService', countyService);

    /* @ngInject */
    function countyService($http, $lodash, $q, exception, SatellizerConfig) {
        var serviceCache = {
            counties: [
                { regionid: 5, id: 1, name: 'Birmingham', status: 1 },
                { regionid: 5, id: 2, name: 'Wolverhampton', status: 1 },
                { regionid: 1, id: 3, name: 'Dorset', status: 1 },
                { regionid: 1, id: 4, name: 'Devon', status: 1 }
            ]
        },
        service = {
            query: query,
            getById: getById,
            bulkRemove: bulkRemove,
            save: save
        };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            return $q.when({
                status: 200,
                data: {
                    counties: angular.copy($lodash.filter(serviceCache.counties, {
                        regionid: parseInt(filter.regionid)
                    })) || [] //no direct updating on cache object
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/counties/'+filter.regionid+'?_=' + new Date().getTime());
        }

        function getById(id) {
            return $q.when({
                status: 200,
                data: {
                    county: angular.copy($lodash.findWhere(serviceCache.counties, { 'id': parseInt(id) }))
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/counties/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(regionid, counties) {
            var count = 0;
            $lodash.forEach(counties, function (item) {
                serviceCache.counties.splice($lodash.findIndex(serviceCache.counties, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/counties/bulkdelete/'
            //  + regionid + '?_=' + new Date().getTime(), counties);
        }

        function save(county) {
            if (!county.id) {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.counties, { 'name': county.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Coounty name conflict! Duplicate County name not Allowed.'
                        }
                    });
                }
                var newCounty = angular.copy(county);
                newCounty.regionid = county.regionid;
                newCounty.status = 1;
                newCounty.id = new Date().getTime();
                serviceCache.counties.push(newCounty);
                return $q.when({
                    status: 200,
                    data: {
                        county: newCounty
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/counties/add/', county)
                //        .then(function (response) {
                //            return response.data;
                //        });
            } else {
                //mockup for simulation
                //Please do validation for duplicate also on edit part
                if ($lodash.findWhere(serviceCache.counties, { 'name': county.name })) {
                    return $q.when({
                        status: 409,
                        data: {
                            message: 'Coounty name conflict! Duplicate County name not Allowed.'
                        }
                    });
                }
                var updateCounty = $lodash.findWhere(serviceCache.counties, { 'id': parseInt(county.id) });
                updateCounty.status = county.status;
                updateCounty.name = county.name;
                return $q.when({
                    status: 200,
                    data: {
                        county: updateCounty
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/counties/update/' + county.id, county)
                //        .then(function (response) {
                //            return response.data;
                //        });
            }
        }
    }
})();
