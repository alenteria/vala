(function () {
    'use strict';

    angular
        .module('app.services')
        .service('uiHelper', uiHelper);

    /* @ngInject */
    function uiHelper(exception) {
        var ajaxLoaderConfig = {
            active: false
        };
        var service = {
            ajaxLoader: ajaxLoader
        };

        return service;

        function ajaxLoader(message) {
            if (message === 'show') {
                return;
            }
        }

    }
})();
