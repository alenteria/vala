(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('questionService', questionService);

    /* @ngInject */
    function questionService($http, $lodash, $q, exception, SatellizerConfig, choiceService) {
        var serviceCache = {
            questions: [
                {
                    id: 1,
                    name: 'Home Region',
                    question: 'In what region do you live?',
                    status: 'enabled',
                    type: 'specific',
                    categoryid: 1,
                    question_type: 'multiple_choice',
                    right_choice_id: 1,
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 2,
                    name: 'Work Region',
                    question: 'In what region do you work?',
                    status: 'enabled',
                    type: 'generic',
                    categoryid: 2,
                    question_type: 'multiple_choice',
                    right_choice_id: 1,
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 3,
                    name: 'Home City',
                    question: 'In what city do you live?',
                    status: 'enabled',
                    type: 'personal_bio_question',
                    categoryid: 3,
                    question_type: 'multiple_choice',
                    right_choice_id: 1,
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 4,
                    name: 'Childrens',
                    question: 'How many kid/kids do you have?',
                    status: 'enabled',
                    type: 'organization_bio_question',
                    categoryid: 4,
                    question_type: 'multiple_choice',
                    right_choice_id: 1,
                    created_at: new Date(),
                    created_by: 1
                }
            ]
        },
            service = {
                query: query,
                getById: getById,
                bulkRemove: bulkRemove,
                save: save
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            //return $http.get(SatellizerConfig.baseUrl + 'questions?_=' + new Date().getTime());
            var questions = angular.copy($lodash.filter(serviceCache.questions, filter));

            return $q.when({
                status: 200,
                data: {
                    questions: questions
                }
            });
        }

        function getById(id) {
            //mockup
            return $q.when({
                status: 200,
                data: {
                    question: angular.copy($lodash.find(serviceCache.questions, { 'id': parseInt(id) }))
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/questions/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(questions) {
            //mockup go to return for backend implementation
            var count = 0;
            $lodash.forEach(questions, function (item) {
                serviceCache.questions.splice($lodash.findIndex(serviceCache.questions, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl
            //+ 'admin/questions/bulkdelete/?_=' + new Date().getTime(), questions);
        }
        function save(question) {
            if (!question.id) {
                //mockup for simulation
                var newType = angular.copy(question);
                newType.id = new Date().getTime();
                serviceCache.questions.push(newType);
                return $q.when({
                    status: 200,
                    data: {
                        question: newType
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/questions/add/', question);
            } else {
                //mockup for simulation
                var updateType = $lodash.findWhere(serviceCache.questions, { 'id': parseInt(question.id) });
                updateType.status = question.status;
                updateType.name = question.name;
                return $q.when({
                    status: 200,
                    data: {
                        question: updateType
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/questions/update/' + question.id, question);
            }
        }
    }
})();
