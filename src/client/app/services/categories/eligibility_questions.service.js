(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('categoryEligibilityQuestionsService', categoryEligibilityQuestionsService);

    /* @ngInject */
    function categoryEligibilityQuestionsService($http, $lodash, $q, exception, SatellizerConfig, questionService) {
        var serviceCache = {
            eligibilityQuestions: [
                {
                    id: 1,
                    order: 1,
                    questionid: 1,
                    categoryid: 1,
                    status: true
                },
                {
                    id: 2,
                    order: 1,
                    questionid: 2,
                    categoryid: 2,
                    status: true
                },
                {
                    id: 3,
                    order: 1,
                    questionid: 3,
                    categoryid: 3,
                    status: true
                },
                {
                    id: 5,
                    order: 2,
                    questionid: 3,
                    categoryid: 3,
                    status: true
                },
                {
                    id: 4,
                    order: 1,
                    questionid: 4,
                    categoryid: 4,
                    status: true
                }
            ]
        },
            service = {
                query: query,
                getById: getById,
                bulkRemove: bulkRemove,
                save: save
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            //return $http.get(SatellizerConfig.baseUrl + 'questions?_=' + new Date().getTime());
            var eligibilityQuestions = angular.copy($lodash.filter(serviceCache.eligibilityQuestions, filter));

            return $q.when({
                status: 200,
                data: {
                    eligibilityQuestions: eligibilityQuestions
                }
            });
        }

        function getById(id) {
            //mockup
            return $q.when({
                status: 200,
                data: {
                    eligibilityQuestion: angular.copy($lodash.find(serviceCache.eligibilityQuestions, { 'id': parseInt(id) }))
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/questions/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(eligibilityQuestions) {
            //mockup go to return for backend implementation
            var count = 0;
            $lodash.forEach(eligibilityQuestions, function (item) {
                serviceCache.eligibilityQuestions.splice(
                    $lodash.findIndex(serviceCache.eligibilityQuestions, 'id', parseInt(item.id)), 1
                );
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl
            //+ 'admin/questions/bulkdelete/?_=' + new Date().getTime(), questions);
        }
        function save(eligibilityQuestion) {
            if (!eligibilityQuestion.id) {
                //mockup for simulation
                var newType = angular.copy(eligibilityQuestion);
                newType.id = new Date().getTime();
                serviceCache.eligibilityQuestions.push(newType);
                return $q.when({
                    status: 200,
                    data: {
                        eligibilityQuestion: newType
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/questions/add/', question);
            } else {
                //mockup for simulation
                var updateType = $lodash.findWhere(
                        serviceCache.eligibilityQuestions, { 'id': parseInt(eligibilityQuestion.id) }
                    );
                updateType.order = eligibilityQuestion.order;
                updateType.questionid = eligibilityQuestion.questionid;
                return $q.when({
                    status: 200,
                    data: {
                        eligibilityQuestion: eligibilityQuestion
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/questions/update/' + question.id, question);
            }
        }
    }
})();
