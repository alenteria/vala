(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('categoryService', categoryService);

    /* @ngInject */
    function categoryService($http, $lodash, $q, exception, SatellizerConfig, choiceService) {
        var serviceCache = {
            categories: [
                {
                    id: 1,
                    name: 'Manager of the year',
                    tag_line: 'An outstanding manager(male or female)',
                    short_name: '',
                    color: '#F19D9D',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 2,
                    name: 'Marketing & PR Award',
                    tag_line: 'For business that stands out',
                    short_name: '',
                    color: '#F19D9D',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 3,
                    name: 'Director of the Year',
                    tag_line: 'An outstanding Director or Board member in a company with more than 10 employees',
                    short_name: '',
                    color: '#F19D9D',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 4,
                    name: 'Employee of the year',
                    tag_line: 'A business with 10 or more employees',
                    short_name: '',
                    color: '#F19D9D',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 5,
                    name: 'Manager of the year',
                    tag_line: 'An outstanding manager(male or female)',
                    short_name: '',
                    color: '#F19D9D',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 6,
                    name: 'Marketing & PR Award',
                    tag_line: 'For business that stands out',
                    short_name: '',
                    color: '#F19D9D',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 7,
                    name: 'Director of the Year',
                    tag_line: 'An outstanding Director or Board member in a company with more than 10 employees',
                    short_name: '',
                    color: '#F19D9D',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 8,
                    name: 'Employee of the year',
                    tag_line: 'A business with 10 or more employees',
                    short_name: '',
                    color: '#4617BE',
                    created_at: new Date(),
                    created_by: 1
                }
            ]
        },
            service = {
                query: query,
                getById: getById,
                bulkRemove: bulkRemove,
                remove: remove,
                save: save,
                getCategoryList: getCategoryList,
                getCategory: getCategory
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function getCategory(id) {
            return getById(id);
        }
        function getCategoryList(filter) {
            return query(filter);
        }
        function query(filter) {
            var categories = angular.copy($lodash.filter(serviceCache.categories, filter));

            return $q.when({
                status: 200,
                data: {
                    categories: categories
                }
            });
        }

        function getById(id) {
            //mockup
            return $q.when({
                status: 200,
                data: {
                    category: angular.copy($lodash.find(serviceCache.categories, { 'id': parseInt(id) }))
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/categories/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(categories) {
            //mockup go to return for backend implementation
            var count = 0;
            $lodash.forEach(categories, function (item) {
                serviceCache.categories.splice($lodash.findIndex(serviceCache.categories, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl
            //+ 'admin/categories/bulkdelete/?_=' + new Date().getTime(), categories);
        }
        function remove(category) {
            //mockup go to return for backend implementation
            serviceCache.categories.splice($lodash.findIndex(serviceCache.categories, 'id', parseInt(category.id)), 1);

            return $q.when({
                status: 200,
                data: {
                    count: 1
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl
            //+ 'admin/categories/bulkdelete/?_=' + new Date().getTime(), categories);
        }
        function save(category) {
            if (!category.id) {
                //mockup for simulation
                var newType = angular.copy(category);
                newType.id = new Date().getTime();
                serviceCache.categories.push(newType);
                return $q.when({
                    status: 200,
                    data: {
                        category: newType
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/categories/add/', category);
            } else {
                //mockup for simulation
                var updateType = $lodash.findWhere(serviceCache.categories, { 'id': parseInt(category.id) });
                updateType = updateType || {};
                updateType.name = category.name;
                updateType.tag_line = category.tag_line;
                updateType.color = category.color;
                updateType.short_name = category.short_name;
                return $q.when({
                    status: 200,
                    data: {
                        category: updateType
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/categories/update/' + category.id, category);
            }
        }
    }
})();
