(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('choiceService', choiceService);

    /* @ngInject */
    function choiceService($http, $lodash, $q, exception, SatellizerConfig) {
        var serviceCache = {
            choices: [
                {
                    id: 1,
                    questionid: 1,
                    name: 'Souht East',
                    status: 'enabled',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 2,
                    questionid: 1,
                    name: 'Souht West',
                    status: 'enabled',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 3,
                    questionid: 1,
                    name: 'London',
                    status: 'enabled',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 4,
                    questionid: 4,
                    name: 'I have none',
                    status: 'enabled',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 5,
                    questionid: 4,
                    name: 'I have 1',
                    status: 'enabled',
                    created_at: new Date(),
                    created_by: 1
                },
                {
                    id: 6,
                    questionid: 4,
                    name: 'I have 2 Kids',
                    status: 'enabled',
                    created_at: new Date(),
                    created_by: 1,
                    answer: true
                },
                {
                    id: 7,
                    questionid: 4,
                    name: 'I have more than 3 Kids',
                    status: 'enabled',
                    created_at: new Date(),
                    created_by: 1
                }
            ]
        },
            service = {
                query: query,
                getById: getById,
                bulkRemove: bulkRemove,
                save: save
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            //return $http.get(SatellizerConfig.baseUrl + 'choices?_=' + new Date().getTime());
            var choices = angular.copy($lodash.filter(serviceCache.choices, filter));

            return $q.when({
                status: 200,
                data: {
                    choices: choices
                }
            });
        }

        function getById(id) {
            //mockup
            return $q.when({
                status: 200,
                data: {
                    choice: angular.copy($lodash.find(serviceCache.choices, { 'id': parseInt(id) }))
                }
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/choices/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(choices) {
            //mockup go to return for backend implementation
            var count = 0;
            $lodash.forEach(choices, function (item) {
                serviceCache.choices.splice($lodash.findIndex(serviceCache.choices, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/choices/bulkdelete/?_=' + new Date().getTime(), choices);
        }
        function save(choice) {
            if (!choice.id) {
                //mockup for simulation
                var newType = angular.copy(choice);
                newType.id = new Date().getTime();
                serviceCache.choices.push(newType);
                return $q.when({
                    status: 200,
                    data: {
                        choice: newType
                    }
                });
                //return $http.post(SatellizerConfig.baseUrl + 'admin/choices/add/', choice);
            } else {
                //mockup for simulation
                var updateType = $lodash.findWhere(serviceCache.choices, { 'id': parseInt(choice.id) });
                updateType.status = choice.status;
                updateType.name = choice.name;
                updateType.answer = choice.answer;
                return $q.when({
                    status: 200,
                    data: {
                        choice: updateType
                    }
                });
                //return $http.put(SatellizerConfig.baseUrl + 'admin/choices/update/' + choice.id, choice);
            }
        }
    }
})();
