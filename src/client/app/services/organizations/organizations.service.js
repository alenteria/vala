(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('organizationsService', organizationsService);

    organizationsService.$inject = ['$http', '$q', 'exception', 'SatellizerConfig'];
    /* @ngInject */
    function organizationsService($http, $q, exception, SatellizerConfig) {
        var baseUrl = SatellizerConfig.baseUrl,
            serviceCache = {
                organizations: [
                  { id: 1, name: 'Agriculture' },
                  { id: 2, name: 'Accounting' },
                  { id: 3, name: 'Advertising' },
                  { id: 4, name: 'Aerospace' },
                  { id: 5, name: 'Apparel &amp; Accessories' },
                  { id: 6, name: 'Automotive' },
                  { id: 7, name: 'Banking' },
                  { id: 8, name: 'Broadcasting' },
                  { id: 9, name: 'Brokerage' },
                  { id: 10, name: 'Biotechnology' },
                  { id: 11, name: 'Cargo Handling' },
                  { id: 12, name: 'Chemical' },
                  { id: 13, name: 'Computer' },
                  { id: 14, name: 'Consulting' },
                  { id: 15, name: 'Consumer Products' },
                  { id: 16, name: 'Construction' },
                  { id: 17, name: 'Cosmetics' },
                  { id: 18, name: 'Defense' },
                  { id: 19, name: 'Department Stores' },
                  { id: 20, name: 'Education' },
                  { id: 21, name: 'Electronics' },
                  { id: 22, name: 'Energy' },
                  { id: 23, name: 'Entertainment &amp; Leisure' },
                  { id: 24, name: 'Fashion' },
                  { id: 25, name: 'Financial Services' },
                  { id: 26, name: 'Food &amp; Beverage' },
                  { id: 27, name: 'Government' },
                  { id: 28, name: 'Grocery' },
                  { id: 29, name: 'Health Care' },
                  { id: 30, name: 'Human Resources / Training' },
                  { id: 31, name: 'Industrial engineering' },
                  { id: 32, name: 'Insurance' },
                  { id: 33, name: 'Internet Publishing' },
                  { id: 34, name: 'Investment Banking' },
                  { id: 35, name: 'Legal' },
                  { id: 36, name: 'Marketing' },
                  { id: 37, name: 'Manufacturing' },
                  { id: 38, name: 'Media/Press/Publishing' },
                  { id: 39, name: 'Motion Picture &amp; Video' },
                  { id: 40, name: 'Music' },
                  { id: 41, name: 'Networking' },
                  { id: 42, name: 'Newspaper Publishers' },
                  { id: 43, name: 'Non-profit' },
                  { id: 44, name: 'Online Auctions' },
                  { id: 45, name: 'Pension Funds' },
                  { id: 46, name: 'Pharmaceuticals' },
                  { id: 47, name: 'Private Equity' },
                  { id: 48, name: 'Public Relations' },
                  { id: 49, name: 'Publishing' },
                  { id: 50, name: 'Real Estate' },
                  { id: 51, name: 'Recruitment' },
                  { id: 52, name: 'Retail &amp; Wholesale' },
                  { id: 53, name: 'Security' },
                  { id: 54, name: 'Service' },
                  { id: 55, name: 'Social Media' },
                  { id: 56, name: 'IT Services / Software' },
                  { id: 57, name: 'Sports' },
                  { id: 58, name: 'Technology' },
                  { id: 59, name: 'Telecommunications' },
                  { id: 60, name: 'Television' },
                  { id: 61, name: 'Transportation' },
                  { id: 62, name: 'Venture Capital' }
                ]
            },
            service = {
                query: query,
                updatePicture: updatePicture,
                uploadPicture: uploadPicture
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            //return $http.get(baseUrl + 'organizationss?_=' + new Date().getTime());
            return $q.when({
                status: 200,
                data: serviceCache.organizationss
            });
        }

        function updatePicture(pictureData) {
            //return $http.post(baseUrl + 'user/picture/'+profileData.id, profileData);
            //mock
            return $q.when({
                status: 200
            });
        }

        function uploadPicture(pictureData) {
            //return $http.post(baseUrl + 'user/picture/'+profileData.id, profileData);
            //mock/expected return
            return $q.when({
                status: 200,
                data: {
                    base64: '', //base64 encoded image temporary while not saved the picture
                    url: '' //actual url, might be cdn or google cloud storage when available
                }
            });
        }
    }
})();
