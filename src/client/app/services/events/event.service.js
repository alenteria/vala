(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('eventService', eventService);

    /* @ngInject */
    function eventService($http, $q, exception, SatellizerConfig) {
        var serviceCache = {
            events: null
        },
            service = {
                query: query,
                getById: getById,
                save: save
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            //return $http.get(baseUrl + 'events?_=' + new Date().getTime());
            //the lower the rank, the higher the access
            var mockevents = [
                {
                    id: 1, area: 'Devon', name: 'Applications',
                    start: new Date('Dec 06 2015'), end: new Date('Dec 10 2015')
                },
                {
                    id: 2, area: 'Dorset', name: 'Applications',
                    start: new Date('Dec 20 2015'), end: new Date('Dec 25 2015')
                },
                {
                    id: 3, area: 'Portsmouth', name: 'Applications',
                    start: new Date('Dec 21 2015'), end: new Date('Dec 26 2015')
                },
                {
                    id: 4, area: 'Isle of Wight', name: 'Applications',
                    start: new Date('Dec 25 2015'), end: new Date('Dec 30 2015')
                },
                {
                    id: 5, area: 'Birmingham', name: 'Applications',
                    start: new Date('Dec 25 2015'), end: new Date('Dec 30 2015')
                },
                {
                    id: 6, area: 'Birmingham', name: 'Nominations',
                    start: new Date('Dec 30 2015'), end: new Date('Jan 04 2016')
                },
                {
                    id: 7, area: 'Southampton', name: 'Applications',
                    start: new Date('Jan 01 2016'), end: new Date('Jan 05 2016')
                }
            ];
            return $q.when({
                status: 200,
                data: mockevents
            });
            //return $http.get(SatellizerConfig.baseUrl + 'admin/events?_=' + new Date().getTime());
        }

        function getById(id) {
            //return $http.get(SatellizerConfig.baseUrl + 'admin/events/edit/' + id + '?_=' + new Date().getTime());
            return $q.when({
                status: 200,
                data: {
                    id: 7, area: 'Southampton', areaid: 1, eventtypeid: 1, name: 'Applications 2016',
                    start: new Date('Dec 30 2015'), end: new Date('Jan 05 2016')
                }
            });
        }

        function save(event) {
            return $http.put(SatellizerConfig.baseUrl + 'admin/events/update/' + event.id, event)
                        .then(function (response) {
                            return response.data;
                        });
        }
    }
})();
