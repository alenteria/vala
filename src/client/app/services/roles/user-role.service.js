(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('userRoleService', userRoleService);

    /* @ngInject */
    function userRoleService($http, $lodash, $q, exception, SatellizerConfig) {
        var serviceCache = {
            userroles: [
                {
                    userid: 1, roleid: 1, email: 'superadmin@gmail.com',
                    displayName: 'Super Admin', status: 1, created_at: '01/02/2015'
                },
                {
                    userid: 2, roleid: 1, email: 'admin@gmail.com',
                    displayName: 'Admin', status: 1, created_at: '01/02/2016'
                },
                {
                    userid: 3, roleid: 1, email: 'sponsor@gmail.com',
                    displayName: 'Sponsor', status: 1, created_at: '01/02/2016'
                },
                {
                    userid: 4, roleid: 2, email: 'sponsoraccount@gmail.com',
                    displayName: 'SponsorAccount', status: 1, created_at: '01/02/2016'
                },
                {
                    userid: 5, roleid: 3, email: 'venususer@gmail.com',
                    displayName: 'VenusUser', status: 1, created_at: '01/02/2016'
                },
                {
                    userid: 6, roleid: 4, email: 'judge@gmail.com',
                    displayName: 'Judge', status: 0, created_at: '01/02/2016'
                }
            ]
        },
            service = {
                query: query,
                bulkRemove: bulkRemove
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            //return $http.get(baseUrl + 'roles?_=' + new Date().getTime());
            //the lower the rank, the higher the access
            return $q.when({
                status: 200,
                data: {
                    userroles: angular.copy($lodash.filter(serviceCache.userroles, {
                        roleid: parseInt(filter.roleid)
                    })) || [] //no direct updating on cache object
                }
            });
            //mockup for the mean time
            //return $http.get(SatellizerConfig.baseUrl + 'admin/userroles/'+filter.roleid+'?_=' + new Date().getTime());
        }

        function bulkRemove(users) {
            //mockup go to return for backend implementation
            var count = 0;
            $lodash.forEach(users, function (item) {
                serviceCache.userroles.splice($lodash.findIndex(serviceCache.userroles, {
                    'roleid': parseInt(item.roleid),
                    'userid': parseInt(item.userid)
                }), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/userroles/bulkdelete/'
            //  + regionid + '?_=' + new Date().getTime(), roles);
        }
    }
})();
