(function () {
    'use strict';

    angular
        .module('app.services')
        .factory('roleService', roleService);

    /* @ngInject */
    function roleService($http, $lodash, $q, exception, SatellizerConfig) {
        var serviceCache = {
            roles: [
                {
                    id: 1, rank: 1, system_name: 'superadmin', name: 'SuperAdmin',
                    description: 'Super Admin is the Main Role for the System', status: 1,
                    created_at: '01/02/2015'
                },
                {
                    id: 2, rank: 2, system_name: 'admin', name: 'Admin',
                    description: 'Admin is a limited System Role', status: 1,
                    created_at: '01/02/2016'
                },
                {
                    id: 3, rank: 3, system_name: 'sponsor', name: 'Sponsor',
                    status: 1, created_at: '01/02/2016'
                },
                {
                    id: 4, rank: 4, system_name: 'sponsoraccount', name: 'SponsorAccount',
                    status: 1, created_at: '01/02/2016'
                },
                {
                    id: 5, rank: 5, system_name: 'venususer', name: 'VenusUser', status: 1,
                    created_at: '01/02/2016'
                },
                {
                    id: 6, rank: 6, system_name: 'judge', name: 'Judge', status: 1,
                    created_at: '01/02/2016'
                },
                {
                    id: 7, rank: 7, system_name: 'mainjudge', name: 'MainJudge', status: 1,
                    created_at: '01/02/2016'
                },
                {
                    id: 100, rank: 100, system_name: 'inactiverole', name: 'Inactive Role', status: 0,
                    created_at: '01/02/2016'
                }
            ]
        },
            service = {
                query: query,
                getById: getById,
                bulkRemove: bulkRemove,
                save: save
            };

        return service;

        /*
         *
         * @param cache boolean if true use cache else retrieve from server
         */
        function query(filter) {
            return $http.get(SatellizerConfig.baseUrl + 'roles?_=' + new Date().getTime());
            //the lower the rank, the higher the access
            //return $q.when({
            //    status: 200,
            //    data: {
            //        roles: angular.copy(serviceCache.roles)
            //    }
            //});
            //mockup for the mean time
            //return $http.get(SatellizerConfig.baseUrl + 'admin/roles?_=' + new Date().getTime());
        }

        function getById(id) {
            //mockup
            //return $q.when({
            //    status: 200,
            //    data: {
            //        role: angular.copy($lodash.find(serviceCache.roles, { 'id': parseInt(id) }))
            //    }
            //});
            return $http.get(SatellizerConfig.baseUrl + 'admin/roles/edit/' + id + '?_=' + new Date().getTime());
        }

        function bulkRemove(roles) {
            //mockup go to return for backend implementation
            var count = 0;
            $lodash.forEach(roles, function (item) {
                serviceCache.roles.splice($lodash.findIndex(serviceCache.roles, 'id', parseInt(item.id)), 1);
                count++;
            });

            return $q.when({
                status: 200,
                data: {
                    count: count
                }
            });
            //return $http.delete(SatellizerConfig.baseUrl + 'admin/roles/bulkdelete/?_=' + new Date().getTime(), roles);
        }
        function save(role) {
            return $http.put(SatellizerConfig.baseUrl + 'admin/roles/update/' + role.id, role)
                        .then(function (response) {
                            return response.data;
                        });
        }
    }
})();
