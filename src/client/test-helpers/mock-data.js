/* jshint -W079 */
var mockData = (function () {
    return {
        getMockLogin: getMockLogin,
        getMockUsers: getMockUsers,
        getMockStates: getMockStates
    };

    function getMockStates() {
        return [
            {
                state: 'dashboard',
                config: {
                    url: '/',
                    templateUrl: 'app/dashboard/dashboard.html',
                    title: 'dashboard',
                    settings: {
                        nav: 1,
                        content: '<i class="fa fa-dashboard"></i> Dashboard'
                    }
                }
            }
        ];
    }

    function getMockUsers() {
        return [
            {
                username: 'username', password: 'password', firstName: 'John',
                lastName: 'Papa', age: 25, location: 'Florida'
            },
            {
                username: 'test@test.com.ph', password: 'password', firstName: 'Ward',
                lastName: 'Bell', age: 31, location: 'California'
            }
        ];
    }

    function getMockLogin() {
        var data = {
            username: 'username', password: 'password', firstName: 'John',
            lastName: 'Papa', age: 25, location: 'Florida',
            email: 'test@test.com',
            displayName: 'firstname lastname',
            picture: {
                data: {}
            },
            cover: {}
        };

        return data;
    }
})();
